"""motorreserva URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static


handler404 = 'apps.configuracion.views.error_404'


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('apps.login.urls')),

    path('dashboard/', include('apps.dashboard.urls')),
    path('configura/', include('apps.configuracion.urls')),
    path('edificio/', include('apps.edificio.urls')),
    path('habitacion/', include('apps.habitacion.urls')),
    path('tarifa/', include('apps.tarifa.urls')),
    path('reserva/', include('apps.reserva.urls')),
    path('sitio/', include('apps.sitio.urls')),
    path('motor/', include('apps.motor.urls')),
    path('perfil/', include('apps.perfil.urls')),
    path('reporte/', include('apps.reporte.urls')),
    path('digital/', include('apps.digital.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
