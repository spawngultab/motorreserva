/* DECLARACIONES */
const id_hotel     = document.getElementById('id_hotel').value;
const btnAcept     = document.querySelector('#btnAcept');
const btnGuardar   = document.querySelector('#btn_guardar');
const btnPolitica  = document.querySelector('#btn-politica');
const idEmail      = document.querySelector('#id_email');
const logo2        = !!document.getElementById('#logo2');

/* FUNCIONES */
function cuestionario() {
	/* CAPTURA DE LOS DATOS DEL CUESTIONARIO COVID-19 */
	let radios1            = document.getElementsByName('pregunta1');
	let radios2            = document.getElementsByName('pregunta2');
	let radios3            = document.getElementsByName('pregunta3');
	let radios4            = document.getElementsByName('pregunta4');
	let radios5            = document.getElementsByName('pregunta5');
	let radios6            = document.getElementsByName('pregunta6');
	let radios7            = document.getElementsByName('pregunta7');
	let radios8            = document.getElementsByName('pregunta8');
	let radios9            = document.getElementsByName('pregunta9');
	let radios10           = document.getElementsByName('pregunta10');
	let radios11           = document.getElementsByName('pregunta11');
	let radios12           = document.getElementsByName('pregunta12');
	let radios13           = document.getElementsByName('pregunta13');
	let respuesta3         = document.getElementById('respuesta3').value;
	let respuesta8         = document.getElementById('respuesta8').value;
	let respuesta11        = document.getElementById('respuesta11').value;
	let respuesta13        = document.getElementById('respuesta13').value;
	let nombreEmergencia   = document.getElementById('nombreCuestionarioEmergencia').value;
	let telefonoEmergencia = document.getElementById('telefonoCuestionarioEmergencia').value;
	let pregunta1, pregunta2, pregunta3, pregunta4, pregunta5, pregunta6, pregunta7, pregunta8, pregunta9, pregunta10, pregunta11, pregunta12, pregunta13;

	for (let i = 0, length = radios1.length; i < length; i++) {
		if (radios1[i].checked) {
			pregunta1 = radios1[i].value;
			break;
		}
	}

	for (let i = 0, length = radios2.length; i < length; i++) {
		if (radios2[i].checked) {
			pregunta2 = radios2[i].value;
			break;
		}
	}

	for (let i = 0, length = radios3.length; i < length; i++) {
		if (radios3[i].checked) {
			pregunta3 = radios3[i].value;
			break;
		}
	}

	for (let i = 0, length = radios4.length; i < length; i++) {
		if (radios4[i].checked) {
			pregunta4 = radios4[i].value;
			break;
		}
	}

	for (let i = 0, length = radios5.length; i < length; i++) {
		if (radios5[i].checked) {
			pregunta5 = radios5[i].value;
			break;
		}
	}

	for (let i = 0, length = radios6.length; i < length; i++) {
		if (radios6[i].checked) {
			pregunta6 = radios6[i].value;
			break;
		}
	}

	for (let i = 0, length = radios7.length; i < length; i++) {
		if (radios7[i].checked) {
			pregunta7 = radios7[i].value;
			break;
		}
	}

	for (let i = 0, length = radios8.length; i < length; i++) {
		if (radios8[i].checked) {
			pregunta8 = radios8[i].value;
			break;
		}
	}

	for (let i = 0, length = radios9.length; i < length; i++) {
		if (radios9[i].checked) {
			pregunta9 = radios9[i].value;
			break;
		}
	}

	for (let i = 0, length = radios10.length; i < length; i++) {
		if (radios10[i].checked) {
			pregunta10 = radios10[i].value;
			break;
		}
	}

	for (let i = 0, length = radios11.length; i < length; i++) {
		if (radios11[i].checked) {
			pregunta11 = radios11[i].value;
			break;
		}
	}

	for (let i = 0, length = radios12.length; i < length; i++) {
		if (radios12[i].checked) {
			pregunta12 = radios12[i].value;
			break;
		}
	}

	for (let i = 0, length = radios13.length; i < length; i++) {
		if (radios13[i].checked) {
			pregunta13 = radios13[i].value;
			break;
		}
	}

	//if (nombreEmergencia != '') {}
	//nombreEmergencia.value = '';
	//telefonoEmergencia.value = '';

	document.getElementById('id_cuestionario').value = pregunta1 + ', ' + pregunta2 + ', ' + pregunta3 + ', ' + respuesta3 + ', ' + pregunta4 + ', ' + pregunta5 + ', ' + pregunta6 + ', ' + pregunta7 + ', ' + pregunta8 + ', ' + respuesta8 + ', ' + pregunta9 + ', ' + pregunta10 + ', ' + pregunta11 + ', ' + respuesta11 + ', ' + pregunta12 + ', ' + pregunta13 + ', ' + respuesta13 + ', ' + nombreEmergencia + ', ' + telefonoEmergencia;
}

function getCookie(name) {
	let cookieValue = null;
	if (document.cookie && document.cookie !== '') {
		const cookies = document.cookie.split(';');
		for (let i = 0; i < cookies.length; i++) {
			const cookie = cookies[i].trim();
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) === (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}

function generaNumeroReserva() {
	let folio             = document.getElementById('folio');
	let numero_prereserva = document.querySelector('#id_numero_prereserva');
	let divisionFolio     = document.querySelector('#divisionFolio');
	let datos             = {id_hotel: id_hotel};

	fetch('/reserva/genera_numero_reserva_directa/', {
		method: 'POST',
		body: JSON.stringify(datos),
		headers: {
			'X-CSRFToken': getCookie('csrftoken'),
			'Content-Type': 'application/json'
		}
	})
	.then((res) => {
		return res.ok ? res.json(): Promise.reject(res)
	})
	.then((json) => {
		folio.innerHTML               = '<span>Folio: </span>' + json;
		numero_prereserva.value       = json;
		divisionFolio.style.marginTop = '0.4rem';
	})
	.catch((err) => {
		let errores      = 'Algo salió mal...';
		folio.innerHTML = 'Error ' + err.status + ': ' + errores;
	});
}

function fechaEntrada() {
	let hoy  = new Date();
	let dia  = hoy.getDate();
	let mes  = parseInt(hoy.getMonth()) + 1;
	let anio = hoy.getFullYear();

	if (dia < 10) {
		dia = '0' + dia
	}

	if (mes < 10) {
		mes = '0' + mes
	}

	let fecha_hoy = anio + '-' + mes + '-' + dia;

	document.getElementById('id_fecha_entrada').value = fecha_hoy;
}

function fechaSalida() {
	let hoy  = new Date();
	let dia  = parseInt(hoy.getDate() + 1);
	let mes  = parseInt(hoy.getMonth()) + 1;
	let anio = hoy.getFullYear();

	if (dia < 10) {
		dia = '0' + dia
	}

	if (dia > 30) {
		dia = '01'
	}

	if (mes < 10) {
		mes = '0' + mes
	}

	let fecha_manana = anio + '-' + mes + '-' + dia;

	document.getElementById('id_fecha_salida').value = fecha_manana;
}

function noches() {
	document.querySelector('#id_fecha_entrada').addEventListener('blur', () => {
		let fechaEntrada = document.getElementById('id_fecha_entrada').value;
		let fechaSalida  = document.getElementById('id_fecha_salida').value;
		let entradaJS    = new Date(fechaEntrada).getTime();
		let salidaJS     = new Date(fechaSalida).getTime();
		let diferencia   = salidaJS - entradaJS;
		let noches;

		if (fechaEntrada == fechaSalida) {
			noches = 1;
		} else {
			noches = Math.round(diferencia / (1000 * 60 * 60 * 24));
		}

		$("#id_noches").val(noches);
	});

	document.querySelector('#id_fecha_salida').addEventListener('blur', () => {
		let fechaEntrada = document.getElementById('id_fecha_entrada').value;
		let fechaSalida  = document.getElementById('id_fecha_salida').value;
		let entradaJS    = new Date(fechaEntrada).getTime();
		let salidaJS     = new Date(fechaSalida).getTime();
		let diferencia   = salidaJS - entradaJS;
		let noches;

		if (fechaEntrada == fechaSalida) {
			noches = 1;
		} else {
			noches = Math.round(diferencia / (1000 * 60 * 60 * 24));
		}

		$("#id_noches").val(noches);
	});
}

function validaMail(email) {
	/*
	let regex = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
	if (!regex.test(email)) {
		return false;
	} else {
	    return true;
	}
	*/
	return true;
}  

function generaFirma() {
	let numero_prereserva = document.getElementById('id_numero_prereserva').value;
	let alerta            = document.querySelector('#alerta');
	let id_firma          = document.querySelector('#id_firma');
	let datos             = {folio: numero_prereserva, id_hotel: id_hotel};

	// CONSULTA DE LA FIRMA MEDIANTE AJAX
	fetch('/reserva/genera_firma_reserva_directa/', {
		method: 'POST',
		body: JSON.stringify(datos),
		headers: {
			'X-CSRFToken': getCookie('csrftoken'),
			'Content-Type': 'application/json'
		}
	})
	.then((res) => {
		return res.ok ? res.json(): Promise.reject(res)
	})
	.then((json) => {
		//btnPolitica.style.visibility = 'hidden';
		alerta.innerHTML             = 'Gracias... <b class="firma">' + json + '</b>';
		id_firma.value               = json;
		btnGuardar.removeAttribute('disabled');
	})
	.catch((err) => {
		let errores      = 'Algo salió mal...';
		alerta.innerHTML = 'Error ' + err.status + ': ' + errores;
	});
}

function validaNumeroPrereserva() {
	let numero_prereserva = document.getElementById('id_numero_prereserva').value;
	let datos             = {folio: numero_prereserva, id_hotel: id_hotel};

	// CONSULTA DEL NUMERO DE RESERVA ACTUAL, MEDIANTE AJAX
	fetch('/reserva/busca_numero_reserva/', {
		method: 'POST',
		body: JSON.stringify(datos),
		headers: {
			'X-CSRFToken': getCookie('csrftoken'),
			'Content-Type': 'application/json'
		}
	})
	.then((res) => {
		return res.ok ? res.json(): Promise.reject(res)
	})
	.then((json) => {
		if (json == true) {
			generaNumeroReserva();
		}
	})
	.catch((err) => {
		console.log(err);
	});
}

/* EJECUCIONES */
// OCULTAMOS LOGO 2
if (logo2 != false) {
	document.getElementById('logo2').style.visibility = 'hidden';
}

document.getElementById('id_noches').value = 1;
document.getElementById('id_cuestionario').value = '';

// MOSTRAMOS LOGO 2
/*if (id_hotel == '1') {
	logo2.style.visibility = 'visible';
}*/

fechaEntrada();
fechaSalida();

noches();

/* VALIDAR EL CORREO */
idEmail.addEventListener('blur', () => {
	let valor     = document.getElementById('id_email').value;
	let resultado = validaMail(valor);
	let email     = document.getElementById('id_email');
	let errorMail = document.getElementById('id_errorMail');

	if (resultado == true) {
		let checkMail       = document.querySelector('#checkMail');
		checkMail.innerHTML = '<i class="fas fa-thumbs-up text-success"></i>';
		errorMail.style.display = 'none';
	} else {
		checkMail.innerHTML = '<i class="fas fa-thumbs-down text-secondary"></i>';
		errorMail.style.display = 'inline-block';
		errorMail.style.display = 'block';
	}
});

generaNumeroReserva();

// BOTON PARA LA APERTURA DEL MODAL
btnPolitica.addEventListener('click', () => {
	let nombre           = document.getElementById('id_nombre').value;
	let apellido_paterno = document.getElementById('id_apellido_paterno').value;
	let apellido_materno = document.getElementById('id_apellido_materno').value;
	let apellidos        = apellido_paterno + ' ' + apellido_materno;
	let telefono         = document.getElementById('id_telefono').value;
	let mail             = document.getElementById('id_email').value;

	document.getElementById('nombreCuestionario').value    = nombre;
	document.getElementById('apellidosCuestionario').value = apellidos;
	document.getElementById('telefonoCuestionario').value  = telefono;
	document.getElementById('correoCuestionario').value    = mail;
});

btnAcept.removeAttribute('disabled');

// GENERAMOS FIRMA DESPUES DEL CLICK EN EL BOTON DE ACEPTAR DE LA VENTANA MODAL
btnAcept.addEventListener('click', () => {
	cuestionario();
	generaFirma();
	validaNumeroPrereserva();
});
