let tabla_rack = document.querySelector("#tabla_rack tr");

let getDaysInMonth = function(month, year) {
    return new Date(year, month, 0).getDate();
};

let dias_junio = getDaysInMonth(6, 2021);
let dias_julio = getDaysInMonth(7, 2021);
let dias_agosto = getDaysInMonth(8, 2021);

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function obtener_fechas() {
  fetch('/reporte/rack_status/')
    .then((res) => res.ok ? res.json(): Promise.reject(res))
    .then((json) => {
        console.log(json);
    })
    .catch((err) => {
        console.log(err)
    })
}

function crea_tabla() {
    let columnas_junio = dias_junio;
    let columnas_julio = dias_julio;
    let columnas_agosto = dias_agosto;

    obtener_fechas();

    tabla_rack.innerHTML = "<th scope='col' class='dato'>No.</th><th scope='col' class='dato'>Tipo</th>";

    for (let c=0; c<columnas_junio; c++) {
        let celda = document.createElement("th"); // Se crea una celda
        celda.innerText = c + 1;
        tabla_rack.appendChild(celda); // Se agrega la celda como columna
    }


    for (let c=0; c<columnas_julio; c++) {
        let celda = document.createElement("th");
        celda.innerText = c + 1;
        tabla_rack.appendChild(celda);
    }

    for (let c=0; c<columnas_agosto; c++) {
        let celda = document.createElement("th");
        celda.innerText = c + 1;
        tabla_rack.appendChild(celda);
    }
}

crea_tabla();
