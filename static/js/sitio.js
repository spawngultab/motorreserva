// Declaraciones

// Funciones
function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function guardar_configuracion(event) {
	event.preventDefault();

	let nombre_configuracion_form = document.getElementById('id_nombre_configuracion').value;
	let color_fondo_form = document.getElementById('id_color_fondo').value;
	let color_fondo_sidebar_form = document.getElementById('id_color_fondo_sidebar').value;
	let color_fondo_navbar_form = document.getElementById('id_color_fondo_navbar').value;
	let color_fondo_footer_form = document.getElementById('id_color_fondo_footer').value;
	let idioma_form = document.getElementById('id_idioma').value;

	let data = {
		"nombre_configuracion_form": nombre_configuracion_form,
		"color_fondo_form": color_fondo_form,
		"color_fondo_sidebar_form": color_fondo_sidebar_form,
		"color_fondo_navbar_form": color_fondo_navbar_form,
		"color_fondo_footer_form": color_fondo_footer_form,
		"idioma_form": idioma_form
	}

	fetch('../sitio/', {
		method: 'POST',
		body: JSON.stringify(data),
		headers: {
			'X-CSRFToken': getCookie('csrftoken'),
			'X-Requested-With': 'XMLHttpRequest',
			'Content-type': 'application/json'
		}
	})
	.then((res) => res.ok ? res.json(): Promise.reject(res))
	.then((json) => {
		console.log(json)
	})
	.catch((err) => {
		console.log('El error es: ', err)
	})
	//.finally(
	//	console.log(' ')
	//)
}

// Ejecuciones
