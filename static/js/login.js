// Declaraciones
let input_username = document.querySelector('#id_username');
let input_password = document.querySelector('#id_password');
let recuerda_mis_datos = document.getElementById('recordar')
let btn_login = document.querySelector('#btn-login');

// Funciones
function recordar(username, pass_encrypt, recuerda_mis_datos) {
	localStorage.setItem('visitado', 'true');
	localStorage.setItem('username', username);
	localStorage.setItem('password', pass_encrypt);

	if (recuerda_mis_datos.checked == true) {
		localStorage.setItem('recordar', 'S');
	} else {
		localStorage.setItem('recordar', 'N');
	}
}

function encryptData(data,iv,key) {
	if (typeof data=="string"){
		data=data.slice();
		encryptedString = CryptoJS.AES.encrypt(data, key, {
			iv: iv,
			mode: CryptoJS.mode.CBC,
			padding: CryptoJS.pad.Pkcs7
		});
	} else {
		encryptedString = CryptoJS.AES.encrypt(JSON.stringify(data), key, {
			iv: iv,
			mode: CryptoJS.mode.CBC,
			padding: CryptoJS.pad.Pkcs7
		});  
	}
	return encryptedString.toString();
}

function decryptData(encrypted,iv,key){
	let decrypted = CryptoJS.AES.decrypt(encrypted, key, {
		iv: iv,
		mode: CryptoJS.mode.CBC,
		padding: CryptoJS.pad.Pkcs7
	});
	return decrypted.toString(CryptoJS.enc.Utf8)
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function envia_datos_usuario(username, password) {
	let data = {
		username: username,
		password: password
	}

	fetch('/', {
    	method: 'POST',
		body: JSON.stringify(data),
		headers: {
			'X-CSRFToken': getCookie('csrftoken'),
			'X-Requested-With': 'XMLHttpRequest',
			'Content-type': 'application/json'
		}
    })
    .then((res) => res.ok ? res.json(): Promise.reject(res))
    .then((json) => {
        console.log(json);
    })
    .catch((err) => {
        console.log(err)
    })
}

// Ejecuciones
if (localStorage.getItem('visitado')) {
	if (localStorage.getItem('recordar')) {
		let recuerdame = localStorage.getItem('username');
		if (recuerdame == 'S') {
			let iv = CryptoJS.enc.Base64.parse("");
			let key = CryptoJS.SHA256("Message");

			input_username.value = localStorage.getItem('username');
			input_password.value = decryptData(localStorage.getItem('username'), iv, key);
			recuerda_mis_datos.value = localStorage.getItem('recordar');
		}
	}
} else {
	input_username.focus();
}

btn_login.addEventListener('click', function(e){
	//e.preventDefault();
	let username = document.getElementById('id_username').value;
	let password = document.getElementById('id_password').value;
	recuerda_mis_datos.value;

	let iv = CryptoJS.enc.Base64.parse("");
	let key = CryptoJS.SHA256("Message");
	let pass_encrypt = encryptData(password, iv, key);

	// Guardar los datos en localStorage
	//recordar(username, pass_encrypt, recuerda_mis_datos);

	//envia_datos_usuario(username, pass_encrypt);
});
