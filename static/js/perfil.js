// Declaraciones
let avatar = document.querySelector(".nav-link #avatar");
let usuario_actual = document.querySelector("#usuario_actual");

// Funciones

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function cargarDatos(usuario) {
    const xhttp = new XMLHttpRequest();
    let nombreArchivo = '/static/js/json/perfil_' + usuario + '.json';    

    xhttp.open('GET', nombreArchivo, true);
    xhttp.send();

    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200) {
            let datos = JSON.parse(this.responseText);
            let usuario_id = datos.usuario_id;
            // DATOS
            avatar.setAttribute('src', '/media/' + datos['avatar']);
            usuario_actual.innerHTML = "<input type='text' name='clave_usuario' id='id_clave_usuario' value='" + datos['clave'] + "' hidden='hidden'>";
        }
    }
}

function obtener_perfil() {
    let usuario = document.getElementById('id_user').value;

    fetch('/dashboard/genera_json/')
    .then((res) => res.ok ? res.json(): Promise.reject(res))
    .then((json) => {
        if (json.data == 'ok') {
            cargarDatos(json.user);
        }
    })
    .catch((err) => {
        console.log(err)
    })
}

// Ejecuciones
obtener_perfil();
