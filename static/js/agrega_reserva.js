let fecha_entrada_form = document.getElementById('id_fecha_entrada');
let fecha_reservacion_form = document.getElementById('id_fecha_reservacion');
let hora_reservacion_form = document.getElementById('id_hora_reservacion');
let deposito_form = document.getElementById('id_deposito');
let numero_reserva_form = document.getElementById('id_numero_reserva');
let numero_reservacion_form = document.getElementById('id_numero_reservacion');
//let clave_usuario = document.querySelector("#usuario_actual #id_clave_usuario");
let fecha_actual = new Date();

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function formato_fecha(fecha) {
	let dia = fecha.getDate();
	let mes = fecha.getMonth() + 1;
	let anio = fecha.getFullYear();

	if (dia < 10) {
		dia = '0' + dia;
	}

	if (mes < 10) {
		mes = '0' + mes;
	}

	let f_completa = anio + '-' + mes + '-' + dia;

	return f_completa
}

function formato_hora(fecha) {
	let horas = fecha.getHours();
	let minutos = fecha.getMinutes();

	if (horas < 10) {
		horas = '0' + horas;
	}

	if (minutos < 10) {
		minutos = '0' + minutos;
	}

	let hora_actual = horas + ':' + minutos;

	return hora_actual
}

function genera_numero_reserva() {
	fetch('/reserva/genera_numero_reserva/')
    .then((res) => res.ok ? res.json(): Promise.reject(res))
    .then((json) => {
        let numero = json.data;
        /* INTEGRAR LA CLAVE DEL USUARIO QUE CAPTURA LA RESERVA, ¡ NO SU PASSWORD ! */
        let longitud_numero = numero.length;
        //let n_reserva = 'W-' + numero;
        numero_reserva_form.innerText = parseInt(numero) + 1;
        numero_reserva_form.value = parseInt(numero) + 1;
        numero_reservacion_form.value = parseInt(numero) + 1;
    })
    .catch((err) => {
        console.log(err)
    })
}

function genera_negativo() {
	let valor_deposito = document.getElementById('id_deposito').value;

	if (valor_deposito.indexOf('-') === 0) {
		deposito_form.value = parseFloat(valor_deposito);
	} else {
		valor_deposito = '-' + valor_deposito;
		deposito_form.value = parseFloat(valor_deposito);
	}

	console.log(typeof(deposito_form.value));
}

fecha_reservacion_form.value = formato_fecha(fecha_actual);
fecha_entrada_form.value = formato_fecha(fecha_actual);
hora_reservacion_form.value = formato_hora(fecha_actual);
deposito_form.addEventListener('blur', genera_negativo, false);
genera_numero_reserva();