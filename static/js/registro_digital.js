// DECLARACIONES
const btnAcept    = document.querySelector('#btnAcept');
const btnGuardar  = document.querySelector('#btn_guardar');
const btnPolitica = document.querySelector('#btn-politica');
const idEmail     = document.querySelector('#id_email');

// FUNCIONES
function getCookie(name) {
	let cookieValue = null;
	if (document.cookie && document.cookie !== '') {
		const cookies = document.cookie.split(';');
		for (let i = 0; i < cookies.length; i++) {
			const cookie = cookies[i].trim();
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) === (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}

function generaNumeroReservaDirecta() {
	let id_hotel          = document.getElementById('id_hotel').value;
	let folio             = document.getElementById('folio');
	let numero_prereserva = document.querySelector('#id_numero_prereserva');
	let divisionFolio     = document.querySelector('#divisionFolio');
	let datos             = {id_hotel: id_hotel};

	fetch('/digital/genera_numero_reserva_directa/', {
		method: 'POST',
		body: JSON.stringify(datos),
		headers: {
			'X-CSRFToken': getCookie('csrftoken'),
			'Content-Type': 'application/json'
		}
	})
	.then((res) => {
		return res.ok ? res.json(): Promise.reject(res)
	})
	.then((json) => {
		console.log(json);
		folio.innerHTML               = '<span>Folio: </span>' + json;
		numero_prereserva.value       = json;
		divisionFolio.style.marginTop = '0.4rem';
	})
	.catch((err) => {
		let errores      = 'Algo salió mal...';
		folio.innerHTML = 'Error ' + err.status + ': ' + errores;
	});
}

//EJECUCIONES
generaNumeroReservaDirecta();
