// Declaraciones
let btn_borrar = document.querySelector('#btn-borrar');
let filtro_selector = document.getElementById('filtro');
let table_body = document.getElementById('table-body');
let moneda = 0;
let adultos = 0;
let ninos = 0;

// Funciones
function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function formato_fecha(fecha) {
    const MESES = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    fecha_separada = fecha.split('-');
    dia = fecha_separada[2].replace(/^(0+)/g, '');
    mes = fecha_separada[1].replace(/^(0+)/g, '');
    fecha_completa = dia + ' de ' + MESES[parseInt(mes) - 1] + ' de ' + fecha_separada[0]
    return fecha_completa
}

function lista_reservas() {
    let opcion = filtro_selector.value;

    fetch('/reserva/?opcion=' + opcion)
    .then((res) => res.ok ? res.json(): Promise.reject(res))
    .then((json) => {
        //console.log(json.lista_reservas.length);
        while (json.lista_reservas) {
            if (json.lista_reservas[0]['moneda'] == 2) {
                moneda = 'Pesos Mexicanos'
            }

            if (json.lista_reservas[0]['adultos'] != 0) {
                adultos = json.lista_reservas[0]['adultos'] + ' Adulto(s)'
            } else {
                adultos = '';
            }

            if (json.lista_reservas[0]['ninos'] != 0) {
                ninos = ' ,' + json.lista_reservas[0]['ninos'] + ' Niño(s)'
            } else {
                ninos = '';
            }

            let fecha_entrada = formato_fecha(json.lista_reservas[0]['fecha_entrada']);
            let fecha_salida = formato_fecha(json.lista_reservas[0]['fecha_salida']);
            let fecha_reservacion = formato_fecha(json.lista_reservas[0]['fecha_reservacion']);

            table_body.innerHTML = `
                <tr>
                    <td>` + json.lista_reservas[0]['apellido_paterno'] + ' ' + json.lista_reservas[0]['apellido_materno'] + ', ' + json.lista_reservas[0]['nombre'] + `</td>
                    <td>` + json.lista_reservas[0]['status'] + ' - <small>' + json.lista_reservas[0]['usuario_reserva'] + `</small></td>
                    <td><small>del <b>` + fecha_entrada + '</b> al <b>' + fecha_salida + `</b></small></td>
                    <td>` + fecha_reservacion + `</td>
                    <td>` + json.lista_reservas[0]['cuartos'] + ' ' + json.lista_reservas[0]['tipo_habitacion__nombre'] + `</td>
                    <td>$ ` + json.lista_reservas[0]['tarifa'] + ' <small>' + moneda + `</small></td>
                    <td><small>` + adultos + ninos + `</small></td>
                    <td>
                        <a class="card-link" href="{% url edita_reserva` + json.lista_reservas[0]['id'] + `%}">
                            <span class="badge bg-primary">Editar <i class="fas fa-edit"></i></span>
                        </a>
                        <a class="card-link" onclick="recuperaId(` + json.lista_reservas[0]['id'] + `)" data-bs-toggle="modal" data-bs-target="#cancelaModal" id="borradoBoton">
                            <span class="badge bg-danger">Cancelar <i class="fas fa-calendar-times"></i></span>
                        </a>
                    </td>
                </tr>
            `;
        }
    })
    .catch((err) => {
        console.log(err)
    })
}

function borra_registro() {
	let dato_modal = document.querySelector("#valor");

	fetch('/reserva/cancelar/?id=' + dato_modal.value)
    .then((res) => res.ok ? res.json(): Promise.reject(res))
    .then((json) => {
    	if (json.data == 'ok') {
    		window.location.reload();
    	}
    })
    .catch((err) => {
        console.log(err)
    })
}

function recuperaId(id) {
	let valor = id;
	let dato_modal = document.getElementById('valor');

	dato_modal.value = valor;
	btn_borrar.addEventListener('click', borra_registro, false);
}

//Ejecuciones
filtro_selector.addEventListener('change', lista_reservas, false);