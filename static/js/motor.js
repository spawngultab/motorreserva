// Declaraciones

// Funciones
// CORREGIR LAS FUNCIONES DE ENTRADA Y SALIDA
function entrada() {
	let hoy = new Date();
	let anio = hoy.getFullYear();
	let mes = parseInt(hoy.getMonth()) + 1;
	let dia = hoy.getDate();

	if (dia < 10) {
		dia = '0' + dia
	}

	if (mes < 10) {
		mes = '0' + mes
	}

	let fecha_hoy = anio + '-' + mes + '-' + dia;

	document.getElementById("id_fecha_entrada").value = fecha_hoy;
}

function salida() {
	let hoy = new Date();
	let anio = hoy.getFullYear();
	let mes = parseInt(hoy.getMonth()) + 1;
	let dia = parseInt(hoy.getDate() + 1);

	if (dia < 10) {
		dia = '0' + dia
	}

	if (dia > 30) {
		dia = '01'
	}

	if (mes < 10) {
		mes = '0' + mes
	}

	let fecha_manana = anio + '-' + mes + '-' + dia;

	document.getElementById('id_fecha_salida').value = fecha_manana;
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function envia_datos() {
    let entrada = document.getElementById("id_fecha_entrada").value;
    let salida = document.getElementById("id_fecha_salida").value;
    let adultos = document.getElementById("id_adultos").value;
	let ninos = document.getElementById("id_ninos").value;

	let data = {
		"entrada": entrada,
		"salida": salida,
		"adultos": adultos,
		"ninos": ninos
	}

    fetch('/reserva/busca_disponibilidad/', {
    	method: 'POST',
		body: JSON.stringify(data),
		headers: {
			'X-CSRFToken': getCookie('csrftoken'),
			'X-Requested-With': 'XMLHttpRequest',
			'Content-type': 'application/json'
		}
    })
    .then((res) => res.ok ? res.json(): Promise.reject(res))
    .then((json) => {
        console.log(json);
    })
    .catch((err) => {
        console.log(err)
    })
}

//Ejecuciones
entrada();
salida();

envia_datos();
