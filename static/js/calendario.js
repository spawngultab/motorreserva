// Declaraciones
let fecha = new Date();

// Funciones
function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function obtener_datos_cliente(info) {
    alert('Event: ' + info.event.title);
}

function obtener_fechas(calendarEl) {
  fetch('/reporte/rack_status/')
    .then((res) => res.ok ? res.json(): Promise.reject(res))
    .then((json) => {
        let data = [json];
        let calendar = new FullCalendar.Calendar(calendarEl, {
          timeZone: 'America/Mexico_City',
          headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth',
          },
          initialDate: fecha,
          locale: 'es',
          editable: true,
          selectable: true,
          businessHours: true,
          dayMaxEvents: true,
          eventClick: function(info) {
            alert('Cliente: ' + info.event.title);
          },
          eventColor: '#378006'
          //eventClick: obtener_datos_cliente(info)
        });
        calendar.render();
        calendar.addEvent({title: 'Huésped Test1', start: '2021-03-12', end: '2021-03-16'});
        calendar.addEvent({title: 'Huésped Test2', start: '2021-03-12', end: '2021-03-16'});
        calendar.addEvent({title: 'Huésped Test3', start: '2021-03-12', end: '2021-03-16'});
        calendar.addEvent({title: 'Huésped Test4', start: '2021-03-10', end: '2021-03-15'});
        calendar.addEvent({title: 'Huésped Test5', start: '2021-03-12', end: '2021-03-16'});
        calendar.addEvent({title: 'Huésped Test6', start: '2021-03-12', end: '2021-03-16'});
        calendar.addEvent({title: 'Huésped Test7', start: '2021-03-12', end: '2021-03-15'});
        calendar.addEvent({title: 'Huésped Test8', start: '2021-03-12', end: '2021-03-14'});
        for (i=0; i<json.length; i++){
          calendar.addEvent(
              {
                  title: json[i].title,
                  start: json[i].start,
                  end: json[i].end
              }
          );
        }
    })
    .catch((err) => {
        console.log(err)
    })
}

// Ejecuciones
document.addEventListener('DOMContentLoaded', function() {
    let calendarEl = document.getElementById('calendar');

    obtener_fechas(calendarEl);
});