#import json
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import Reserva
from .forms import ReservaForm
from apps.habitacion.models import Habitacion
from apps.configuracion.models import DbDatos
from apps.configuracion.forms import DbDatosForm
from apps.configuracion.models import TabEst, TabHt
from apps.motor.views import trae_datos
from apps.edificio.models import Edificio


@login_required
def reserva_view(request):
	hotel = Edificio.objects.all().values('nombre_corto')
	if request.method == 'GET':
		opcion = request.GET.get('opcion')
		if opcion:
			consulta = list(Reserva.objects.filter(status=opcion).order_by('-id').values('id', 'fecha_reservacion', 'numero_reservacion', 'hora_reservacion', 'fecha_entrada', 'fecha_salida', 'tipo_habitacion__nombre', 'cuartos', 'adultos', 'ninos', 'segmento_mercado', 'agencia', 'nombre', 'apellido_paterno', 'apellido_materno', 'correo', 'telefono', 'ciudad', 'estado', 'pais', 'codigo_postal', 'deposito', 'fecha_deposito', 'moneda', 'tarifa', 'comentario', 'usuario_reserva', 'status'))
			return JsonResponse({'lista_reservas': consulta})
		else:
			consulta = list(Reserva.objects.filter(status='R').order_by('-id').values('id', 'fecha_reservacion', 'numero_reservacion', 'hora_reservacion', 'fecha_entrada', 'fecha_salida', 'tipo_habitacion__nombre', 'cuartos', 'adultos', 'ninos', 'segmento_mercado', 'agencia', 'nombre', 'apellido_paterno', 'apellido_materno', 'correo', 'telefono', 'ciudad', 'estado', 'pais', 'codigo_postal', 'deposito', 'fecha_deposito', 'moneda', 'tarifa', 'comentario', 'usuario_reserva', 'status'))
			return render(request, 'configuracion/configura_reserva.html', {'lista_reservas': consulta, 'hotel': hotel})


def numero_reserva(self):
	consulta = Reserva.objects.all().count()
	return JsonResponse({'data': consulta})


@login_required
def lee_segmento_agencia(request):
	consulta = list(TabHt.objects.all().values('id', 'segmento_web', 'agencia_web'))
	segmento_mercado = consulta[0]['segmento_web']
	agencia = consulta[0]['agencia_web']
	data = {
		"segmento": segmento_mercado,
		"agencia": agencia
	}
	return data


def guarda_reserva_cnts(request, status):
	if request.method == 'POST':
		form_DbDatos = DbDatosForm(request.POST)
		tipo_habitacion = list(Habitacion.objects.filter(id=request.POST.get('tipo_habitacion')).values('clave'))
		if form_DbDatos.is_valid():
			formulario = form_DbDatos.save(commit=False)
			formulario.nombreda = request.POST.get('apellido_paterno') + ' ' + request.POST.get('apellido_materno') + ' ' + request.POST.get('nombre')
			formulario.statusda = status
			formulario.ciudadda = request.POST.get('ciudad')
			formulario.cupon = request.POST.get('numero_reservacion')
			formulario.fhaentda = request.POST.get('fecha_entrada')
			formulario.fhasalda = request.POST.get('fecha_salida')
			formulario.observda = request.POST.get('comentario')
			formulario.paisda = request.POST.get('pais')
			formulario.deposida = request.POST.get('deposito')
			formulario.fhadepda = request.POST.get('fecha_deposito')
			formulario.numperda = request.POST.get('adultos')
			formulario.tpocam1da = request.POST.get('moneda')
			formulario.cveestda1 = tipo_habitacion[0]['clave']
			formulario.cveestda2 = request.POST.get('agencia')
			formulario.cveestda4 = request.POST.get('segmento_mercado')
			formulario.estadoda = request.POST.get('estado')
			formulario.tarifada = request.POST.get('tarifa')
			formulario.numcuada = request.POST.get('cuartos')
			formulario.fharesda = request.POST.get('fecha_reservacion')
			formulario.iniresda = request.POST.get('usuario_reserva')
			formulario.horaresda = request.POST.get('hora_reservacion')
			formulario.numninda = request.POST.get('ninos')
			formulario.nombr_da = request.POST.get('nombre')
			formulario.appat_da = request.POST.get('apellido_paterno')
			formulario.apmat_da = request.POST.get('apellido_materno')
			formulario.email_da = request.POST.get('correo')
			formulario.codposda = request.POST.get('codigo_postal')
			formulario.telefoda = request.POST.get('telefono')
			formulario.save()


def cancela_reserva_cnts(request):
	status = 'C'
	if request.method == 'GET':
		form_DbDatos = DbDatosForm(request.GET)
		reserva_id = request.GET.get('id')
		consulta = list(Reserva.objects.filter(id=reserva_id).values('fecha_reservacion', 'numero_reservacion', 'hora_reservacion', 'fecha_entrada', 'fecha_salida', 'tipo_habitacion', 'cuartos', 'adultos', 'ninos', 'segmento_mercado', 'agencia', 'nombre', 'apellido_paterno', 'apellido_materno', 'correo', 'telefono', 'ciudad', 'estado', 'pais', 'codigo_postal', 'deposito', 'fecha_deposito', 'moneda', 'tarifa', 'comentario', 'usuario_reserva', 'status'))
		formulario = form_DbDatos.save(commit=False)
		formulario.nombreda = consulta[0]['apellido_paterno'] + ' ' + consulta[0]['apellido_materno'] + ' ' + consulta[0]['nombre']
		formulario.statusda = status
		formulario.ciudadda = consulta[0]['ciudad']
		formulario.cupon = consulta[0]['numero_reservacion']
		formulario.fhaentda = consulta[0]['fecha_entrada']
		formulario.fhasalda = consulta[0]['fecha_salida']
		formulario.observda = consulta[0]['comentario']
		formulario.paisda = consulta[0]['pais']
		formulario.deposida = consulta[0]['deposito']
		formulario.fhadepda = consulta[0]['fecha_deposito']
		formulario.numperda = consulta[0]['adultos']
		formulario.tpocam1da = consulta[0]['moneda']
		formulario.cveestda1 = consulta[0]['tipo_habitacion']
		formulario.cveestda2 = consulta[0]['agencia']
		formulario.cveestda4 = consulta[0]['segmento_mercado']
		formulario.estadoda = consulta[0]['estado']
		formulario.tarifada = consulta[0]['tarifa']
		formulario.numcuada = consulta[0]['cuartos']
		formulario.fharesda = consulta[0]['fecha_reservacion']
		formulario.iniresda = consulta[0]['usuario_reserva']
		formulario.horaresda = consulta[0]['hora_reservacion']
		formulario.numninda = consulta[0]['ninos']
		formulario.nombr_da = consulta[0]['nombre']
		formulario.appat_da = consulta[0]['apellido_paterno']
		formulario.apmat_da = consulta[0]['apellido_materno']
		formulario.email_da = consulta[0]['correo']
		formulario.codposda = consulta[0]['codigo_postal']
		formulario.telefoda = consulta[0]['telefono']
		print(consulta[0]['tipo_habitacion'])
		formulario.save()


@login_required
def agrega_reserva_view(request):
	status = request.POST.get('status')
	if request.method == 'POST':
		form_Reserva = ReservaForm(request.POST)
		if form_Reserva.is_valid():
			form_Reserva.save()
			guarda_reserva_cnts(request, status)
			return redirect('reserva')
	else:
		form_Reserva = ReservaForm()
	return render(request, 'configuracion/agrega_reserva.html', {'form': form_Reserva, 'datos': lee_segmento_agencia(request)})


@login_required
def cancela_reserva_view(request):
	if request.method == 'GET':
		valor = request.GET.get('id')
		Reserva.objects.filter(id=valor).update(status='C')
		cancela_reserva_cnts(request)
	return JsonResponse({'data': 'ok'})


@login_required
def edita_reserva_view(request, id):
	status = 'M'
	guarda_reserva_cnts(request, status)
	reserva = Reserva.objects.get(id=id)
	if request.method == 'POST':
		form = ReservaForm(request.POST, instance=reserva)
		if form.is_valid():
			form.save()
			return redirect('reserva')
	else:
		form = ReservaForm(instance=reserva)
	return render(request, 'configuracion/edita_reserva.html', {'form': form})

# MOTOR DE RESERVACIONES: Inicia reserva
def busca_disponibilidad_view(request):
	return render(request, 'reserva/disponibilidad.html', {'datos': trae_datos()})


def reserva_motor(request):
	status = request.POST.get('status')
	if request.method == 'POST':
		form_Reserva = ReservaForm(request.POST)
		if form_Reserva.is_valid():
			form_Reserva.save()
			#guarda_reserva_cnts(request, status)
			#return redirect('reserva')
	else:
		form_Reserva = ReservaForm()
	return render(request, 'reserva/reservar.html', {'form': form_Reserva, 'datos': trae_datos()})


def confirma_motor(request):
	return render(request, 'reserva/confirmar.html', {'datos': trae_datos()})
