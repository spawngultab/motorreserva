from django.db import models
from apps.habitacion.models import Habitacion
from apps.edificio.models import Edificio


class Reserva(models.Model):
	TIPO_CAMBIO = (
		(0, 'Dólares'),
		(1, 'Euros'),
		(2, 'Pesos Mexicanos'),
	)

	fecha_reservacion = models.DateField(default='') # fharesda
	numero_reservacion = models.CharField(max_length=8, blank=True, null=True) # numresda
	hora_reservacion = models.CharField(max_length=8, default='') # horaresda
	fecha_entrada = models.DateField() # fhaentda
	fecha_salida = models.DateField() # fhasalda
	tipo_habitacion = models.ForeignKey(Habitacion, on_delete=models.CASCADE) # cveestda1
	cuartos = models.IntegerField() # numcuada
	adultos = models.IntegerField() # numperda
	ninos = models.IntegerField() # numninda
	segmento_mercado = models.CharField(max_length=5, blank=True, null=True)  # cveestda4
	agencia = models.CharField(max_length=5, blank=True, null=True) # cveestda2
	nombre = models.CharField(max_length=100, default='') # nombr_da
	apellido_paterno = models.CharField(max_length=100, default='') # appat_da
	apellido_materno = models.CharField(max_length=100, blank=True, null=True) # apmat_da
	correo = models.CharField(max_length=200, default='') # email_da
	telefono = models.CharField(max_length=18, default='') # telefoda
	ciudad = models.CharField(max_length=80, default='', blank=True, null=True) # ciudadda
	estado = models.CharField(max_length=80, default='', blank=True, null=True) # estadoda
	pais = models.CharField(max_length=50, default='', blank=True, null=True) # paisda
	codigo_postal = models.CharField(max_length=10, default='', blank=True, null=True) # codposda
	deposito = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True) # deposida
	fecha_deposito = models.DateField(blank=True, null=True) # fhadepda
	moneda = models.IntegerField(choices=TIPO_CAMBIO, blank=True, null=True) # tpocam1da
	tarifa = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True) # tarifada
	comentario = models.TextField(default='', blank=True, null=True) # observda
	usuario_reserva = models.CharField(max_length=4, default='WEB', blank=True, null=True) # iniresda
	status = models.CharField(max_length=1, default='R', blank=True, null=True) # statusda
	registro_creado = models.DateTimeField(auto_now_add=True)
	registro_actualizado = models.DateTimeField(auto_now=True)
	hotel = models.ForeignKey(Edificio, on_delete=models.CASCADE, blank=True, null=True)

	class Meta:
		verbose_name = 'Reserva'
		verbose_name_plural = 'Reservaciones'

	def __str__(self):
		return '%s %s %s' % (self.nombre, self.apellido_paterno, self.apellido_materno)
