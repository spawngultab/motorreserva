from django import forms
from .models import Reserva


class ReservaForm(forms.ModelForm):
	class Meta:
		model = Reserva
		fields = '__all__'
		widgets = {
			'fecha_reservacion': forms.TextInput(attrs={'class': 'form-control', 'type': 'date', 'hidden': 'hidden'}),
			'numero_reservacion': forms.TextInput(attrs={'class': 'form-control numero_reserva', 'readonly': 'readonly'}),
			'hora_reservacion': forms.TextInput(attrs={'class': 'form-control', 'hidden': 'hidden'}),
			'fecha_entrada': forms.TextInput(attrs={'class': 'form-control fecha', 'type': 'date'}),
			'fecha_salida': forms.TextInput(attrs={'class': 'form-control fecha', 'type': 'date'}),
			'tipo_habitacion': forms.Select(attrs={'class': 'form-select cincuenta'}),
			'cuartos': forms.TextInput(attrs={'class': 'form-control cincuenta'}),
			'adultos': forms.TextInput(attrs={'class': 'form-control'}),
			'ninos': forms.TextInput(attrs={'class': 'form-control'}),
			'segmento_mercado': forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
			'agencia': forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
			'nombre': forms.TextInput(attrs={'class': 'form-control nombapell', 'placeholder': 'Nombre'}),
			'apellido_paterno': forms.TextInput(attrs={'class': 'form-control nombapell', 'placeholder': 'Apellido Paterno'}),
			'apellido_materno': forms.TextInput(attrs={'class': 'form-control nombapell', 'placeholder': 'Apellido Materno'}),
			'correo': forms.TextInput(attrs={'class': 'form-control treinta'}),
			'telefono': forms.TextInput(attrs={'class': 'form-control treinta'}),
			'ciudad': forms.TextInput(attrs={'class': 'form-control treinta', 'placeholder': 'Ciudad'}),
			'estado': forms.TextInput(attrs={'class': 'form-control treinta', 'placeholder': 'Estado'}),
			'pais': forms.TextInput(attrs={'class': 'form-control treinta', 'placeholder': 'País'}),
			'codigo_postal': forms.TextInput(attrs={'class': 'form-control treinta'}),
			'deposito': forms.NumberInput(attrs={'class': 'form-control', 'step': '0.01', 'value': '0.00'}),
			'fecha_deposito': forms.TextInput(attrs={'class': 'form-control', 'type': 'date'}),
			'moneda': forms.Select(attrs={'class': 'form-select'}),
			'tarifa': forms.NumberInput(attrs={'class': 'form-control', 'step': '0.01', 'value': '0.00'}),
			'comentario': forms.Textarea(attrs={'class': 'form-control'}),
			'usuario_reserva': forms.TextInput(attrs={'class': 'form-control', 'value': 'WEB', 'hidden': 'hidden'}),
			'status': forms.TextInput(attrs={'class': 'form-control', 'value': 'R', 'hidden': 'hidden'}),
			'hotel': forms.NumberInput(attrs={'class': 'form-control'}),
		}

