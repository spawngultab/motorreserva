from django.urls import path
from .views import *


urlpatterns = [
    path('', reserva_view, name='reserva'),
    path('agregar/', agrega_reserva_view, name='agrega_reserva'),
    path('editar/<int:id>/', edita_reserva_view, name='edita_reserva'),
    path('cancelar/', cancela_reserva_view, name='cancela_reserva'),
    path('buscar_disponibilidad/', busca_disponibilidad_view, name='busca_disponibilidad'),

    path('genera_numero_reserva/', numero_reserva, name="genera_numero_reserva"),
    path('reservar/', reserva_motor, name="reservar"),
    path('confirmar/', confirma_motor, name="confirmar"),
]
