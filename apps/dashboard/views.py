import json
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import JsonResponse
from apps.perfil.models import Perfil


@login_required
def dashboard_view(request):
	return render(request, 'dashboard/dashboard.html')


@login_required
def genera_json_view(request):
	usuario = request.user
	consulta = list(Perfil.objects.filter(usuario=usuario).values('id', 'usuario_id', 'usuario_id__username', 'clave', 'tipo', 'avatar', 'puesto', 'direccion', 'pais', 'telefono', 'hotel__nombre_corto'))

	datos = {
		"id": consulta[0]['id'],
		"usuario_id": consulta[0]['usuario_id'],
		"usuario": consulta[0]['usuario_id__username'],
		"clave": consulta[0]['clave'],
		"tipo": consulta[0]['tipo'],
		"avatar": consulta[0]['avatar'],
		"puesto": consulta[0]['puesto'],
		"direccion": consulta[0]['direccion'],
		"pais": consulta[0]['pais'],
		"telefono": consulta[0]['telefono'],
		"hotel": consulta[0]['hotel__nombre_corto']
	}

	with open('static/js/json/perfil_' + str(consulta[0]['id']) + '.json', 'w') as f:
		json.dump(datos, f, ensure_ascii=False)
		generado = True

	if generado:
		respuesta = {
			"data": "ok",
			"user": consulta[0]['usuario_id']
		}
	else:
		respuesta = {
			"data": "fail"
		}
	return JsonResponse(respuesta, safe=False)
