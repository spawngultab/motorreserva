from django.urls import path
from . import views


urlpatterns = [
    path('', views.dashboard_view, name='dashboard'),
    path('genera_json/', views.genera_json_view, name='genera_json'),
]
