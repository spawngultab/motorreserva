import json
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.core import serializers
from .models import Motor
from .forms import MotorForm
from apps.edificio.models import Edificio


@login_required
def motor_view(request):
	consulta = list(Motor.objects.all().values())
	return render(request, 'configuracion/configura_motor.html', {'motor': consulta})


@login_required
def agrega_motor_view(request):
	if request.method == 'POST':
		form = MotorForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('motor')
	else:
		form = MotorForm()
	return render(request, 'configuracion/agrega_motor.html', {'form': form})


@login_required
def edita_motor_view(request, id):
	motor = Motor.objects.get(id=id)
	if request.method == 'POST':
		form = MotorForm(request.POST, instance=motor)
		if form.is_valid():
			form.save()
			return redirect('motor')
	else:
		form=MotorForm(instance=motor)
	return render(request, 'configuracion/edita_motor.html', {'form': form})


@login_required
def borra_motor_view(request, id):
	Motor.objects.filter(id=id).delete()
	return redirect('motor')


def trae_datos():
	consulta_motor = list(Motor.objects.all().values())
	consulta_edificio = list(Edificio.objects.all().values())

	data = {}
	arreglo = []

	for i in range(len(consulta_motor)):
		dato = {
			'motor_id': consulta_motor[0]['id'],
			'motor_nombre': consulta_motor[0]['nombre'],
			'motor_opcion_menu': consulta_motor[0]['opcion_menu'],
			'motor_direccion_menu': consulta_motor[0]['direccion_menu'],
			'motor_ubicacion_fotos': consulta_motor[0]['ubicacion_fotos'],
			'motor_politicas_reservacion': consulta_motor[0]['politicas_reservacion'],
			'edificio_id': consulta_edificio[0]['id'],
			'edificio_nombre': consulta_edificio[0]['nombre'],
			'edificio_direccion': consulta_edificio[0]['direccion'],
			'edificio_correo': consulta_edificio[0]['correo'],
			'edificio_telefono': consulta_edificio[0]['telefono'],
			'edificio_web': consulta_edificio[0]['web'],
			'edificio_facebook': consulta_edificio[0]['facebook'],
			'edificio_twitter': consulta_edificio[0]['twitter'],
			'edificio_instagram': consulta_edificio[0]['instagram'],
			'edificio_youtube': consulta_edificio[0]['youtube'],
			'edificio_foto': consulta_edificio[0]['foto'],
			'edificio_logo': consulta_edificio[0]['logo'],
			'edificio_banner': consulta_edificio[0]['banner'],
		}
		data.update(dato)
	arreglo.append(dato)
	return arreglo

def lanza_motor_view(request):
	return render(request, 'motor/busca_fecha.html', {'consulta': trae_datos()})
