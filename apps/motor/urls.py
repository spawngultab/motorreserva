from django.urls import path
from .views import *


urlpatterns = [
    path('', motor_view, name='motor'),
    path('agregar/', agrega_motor_view, name='agrega_motor'),
    path('editar/<int:id>/', edita_motor_view, name='edita_motor'),
    path('borrar/<int:id>/', borra_motor_view, name='borra_motor'),
    path('busca_fecha/', lanza_motor_view, name='lanza_preview'),
]
