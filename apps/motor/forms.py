from django import forms
from ckeditor.widgets import CKEditorWidget
from .models import Motor


class MotorForm(forms.ModelForm):
	class Meta:
		model = Motor
		fields = '__all__'
		widgets = {
			'nombre': forms.TextInput(attrs={'class': 'form-control'}),
			'opcion_menu': forms.TextInput(attrs={'class': 'form-control'}),
			'direccion_menu': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'https://algun-sitio.com'}),
			'ubicacion_fotos': forms.TextInput(attrs={'class': 'form-control'}),
			'politicas_reservacion': CKEditorWidget(config_name='default'),
		}
