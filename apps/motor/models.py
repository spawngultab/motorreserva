from django.db import models
from ckeditor.fields import RichTextField


class Motor(models.Model):
	nombre = models.CharField(max_length=20, blank=True, null=True)
	opcion_menu = models.CharField(max_length=20, default='')
	direccion_menu = models.URLField(blank=True, null=True)
	ubicacion_fotos = models.CharField(max_length=500)
	politicas_reservacion = RichTextField(config_name='default', blank=True, null=True)

	class Meta:
		verbose_name = 'Motor'
		verbose_name_plural = 'Motor'

	def __str__(self):
		return self.nombre
