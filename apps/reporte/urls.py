from django.urls import path
from .views import *


urlpatterns = [
    path('', rack_view, name='rack'),
    path('rack_status/', rack_status, name='rack_status')
]
