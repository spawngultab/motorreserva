from datetime import date, datetime, timedelta
from django.shortcuts import render
from django.http import JsonResponse
from apps.configuracion.models import DbDatos


def rack_status(request):
	consulta_reservas = list(DbDatos.objects.filter(statusda='R').values('nombreda', 'fhaentda', 'fhasalda', 'numhabda'))
	data = {}
	arreglo = []
	for i in range(len(consulta_reservas)):
		if consulta_reservas[i]['fhasalda'] != None:
			salida = consulta_reservas[i]['fhasalda']
			nueva_salida = salida + timedelta(days=1)

			dato = {
				'title': consulta_reservas[i]['nombreda'],
				'start': consulta_reservas[i]['fhaentda'],
				'end': nueva_salida
			}
			data.update(dato)
		arreglo.append(dato)
	return JsonResponse(arreglo, safe=False)


def rack_view(request):
	return render(request, 'reporte/rack.html')
