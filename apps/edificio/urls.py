from django.urls import path
from .views import *


urlpatterns = [
    path('', edificio_view, name='edificio'),
    path('agregar/', agrega_edificio_view, name='agrega_edificio'),
    path('editar/<int:id>/', edita_edificio_view, name='edita_edificio'),
]
