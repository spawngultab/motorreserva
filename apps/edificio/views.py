from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Edificio
from .forms import EdificioForm


@login_required
def edificio_view(request):
	consulta = list(Edificio.objects.all().values())
	return render(request, 'configuracion/configura_edificio.html', {'consulta': consulta})


@login_required
def agrega_edificio_view(request):
	if request.method == 'POST':
		form = EdificioForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('edificio')
	else:
		form = EdificioForm()
	return render(request, 'configuracion/agrega_edificio.html', {'form': form})


def edita_edificio_view(request, id):
	edificio = Edificio.objects.get(id=id)
	if request.method == 'POST':
		form = EdificioForm(request.POST, request.FILES, instance=edificio)
		if form.is_valid():
			form.save()
			return redirect('edificio')
	else:
		form = EdificioForm(instance=edificio)
	return render(request, 'configuracion/edita_edificio.html', {'form': form})
