from django import forms
from .models import Edificio


class EdificioForm(forms.ModelForm):
	class Meta:
		model = Edificio
		fields = '__all__'
		widgets = {
			'nombre': forms.TextInput(attrs={'class': 'form-control'}),
			'nombre_corto': forms.TextInput(attrs={'class': 'form-control'}),
			'direccion': forms.TextInput(attrs={'class': 'form-control'}),
			'estado': forms.TextInput(attrs={'class': 'form-control'}),
			'correo': forms.TextInput(attrs={'class': 'form-control'}),
			'telefono': forms.TextInput(attrs={'class': 'form-control'}),
			'web': forms.TextInput(attrs={'class': 'form-control'}),
			'facebook': forms.TextInput(attrs={'class': 'form-control'}),
			'twitter': forms.TextInput(attrs={'class': 'form-control'}),
			'instagram': forms.TextInput(attrs={'class': 'form-control'}),
			'youtube': forms.TextInput(attrs={'class': 'form-control'}),
			'foto': forms.ClearableFileInput(attrs={'class': 'form-control'}),
			'logo': forms.ClearableFileInput(attrs={'class': 'form-control'}),
			'banner': forms.ClearableFileInput(attrs={'class': 'form-control'}),
			'background_reserva_directa': forms.ClearableFileInput(attrs={'class': 'form-control'}),
		}
