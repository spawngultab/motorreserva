from django.db import models


class Edificio(models.Model):
	nombre = models.CharField(max_length=200)
	nombre_corto = models.CharField(max_length=50, blank=True, null=True)
	direccion = models.CharField(max_length=500)
	estado = models.CharField(max_length=80, blank=True, null=True)
	correo = models.CharField(max_length=90)
	telefono = models.CharField(max_length=20)
	web = models.URLField(blank=True, null=True)
	facebook = models.URLField(blank=True, null=True)
	twitter = models.URLField(blank=True, null=True)
	instagram = models.URLField(blank=True, null=True)
	youtube = models.URLField(blank=True, null=True)
	foto = models.ImageField(upload_to='edificio/', blank=True, null=True)
	logo = models.ImageField(upload_to='edificio/logo/', blank=True, null=True)
	banner = models.ImageField(upload_to='edificio/banner/', blank=True, null=True)
	background_reserva_directa = models.ImageField(upload_to='edificio/background/', blank=True, null=True)

	class Meta:
		verbose_name = 'Edificio'
		verbose_name_plural = 'Edificios'

	def __str__(self):
		return self.nombre
