# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class DbCapct(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    cgrupo = models.IntegerField(db_column='CGRUPO', blank=True, null=True)  # Field name made lowercase.
    cnumero = models.IntegerField(db_column='CNUMERO', blank=True, null=True)  # Field name made lowercase.
    creser = models.IntegerField(db_column='CRESER', blank=True, null=True)  # Field name made lowercase.
    ccuarto = models.SmallIntegerField(db_column='CCUARTO', blank=True, null=True)  # Field name made lowercase.
    ccuenta = models.IntegerField(db_column='CCUENTA', blank=True, null=True)  # Field name made lowercase.
    ccargo = models.DecimalField(db_column='CCARGO', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    ccredito = models.DecimalField(db_column='CCREDITO', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    cstatus = models.IntegerField(db_column='CSTATUS', blank=True, null=True)  # Field name made lowercase.
    cfecha = models.DateField(db_column='CFECHA', blank=True, null=True)  # Field name made lowercase.
    crefer = models.CharField(db_column='CREFER', max_length=120, blank=True, null=True)  # Field name made lowercase.
    cpase = models.CharField(db_column='CPASE', max_length=4, blank=True, null=True)  # Field name made lowercase.
    cage = models.CharField(db_column='CAGE', max_length=3, blank=True, null=True)  # Field name made lowercase.
    cfactura = models.IntegerField(db_column='CFACTURA', blank=True, null=True)  # Field name made lowercase.
    cnumhues = models.IntegerField(db_column='CNUMHUES', blank=True, null=True)  # Field name made lowercase.
    chora = models.CharField(db_column='CHORA', max_length=8, blank=True, null=True)  # Field name made lowercase.
    crefnum = models.IntegerField(db_column='CREFNUM', blank=True, null=True)  # Field name made lowercase.
    chabt = models.SmallIntegerField(db_column='CHABT', blank=True, null=True)  # Field name made lowercase.
    chuest = models.IntegerField(db_column='CHUEST', blank=True, null=True)  # Field name made lowercase.
    cpaqte = models.CharField(db_column='CPAQTE', max_length=6, blank=True, null=True)  # Field name made lowercase.
    cfolpaq = models.IntegerField(db_column='CFOLPAQ', blank=True, null=True)  # Field name made lowercase.
    ctipmov = models.CharField(db_column='CTIPMOV', max_length=1, blank=True, null=True)  # Field name made lowercase.
    folio_pago = models.IntegerField(db_column='FOLIO_PAGO', blank=True, null=True)  # Field name made lowercase.
    estatus = models.CharField(db_column='ESTATUS', max_length=1, blank=True, null=True)  # Field name made lowercase.
    cxc = models.CharField(db_column='CXC', max_length=1, blank=True, null=True)  # Field name made lowercase.
    ctadep = models.IntegerField(db_column='CTADEP', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DB_CAPCT'


class DbDatos(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nombreda = models.CharField(db_column='NOMBREDA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    statusda = models.CharField(db_column='STATUSDA', max_length=1, blank=True, null=True)  # Field name made lowercase.
    ciudadda = models.CharField(db_column='CIUDADDA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    numresda = models.IntegerField(db_column='NUMRESDA', blank=True, null=True)  # Field name made lowercase.
    fhaentda = models.DateField(db_column='FHAENTDA', blank=True, null=True)  # Field name made lowercase.
    fhasalda = models.DateField(db_column='FHASALDA', blank=True, null=True)  # Field name made lowercase.
    fhaextda = models.DateField(db_column='FHAEXTDA', blank=True, null=True)  # Field name made lowercase.
    numhabda = models.IntegerField(db_column='NUMHABDA', blank=True, null=True)  # Field name made lowercase.
    planvida = models.CharField(db_column='PLANVIDA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    observda = models.CharField(db_column='OBSERVDA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    observda2 = models.CharField(db_column='OBSERVDA2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    dirda = models.CharField(db_column='DIRDA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    telda = models.CharField(db_column='TELDA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    paisda = models.CharField(db_column='PAISDA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    formpada = models.CharField(db_column='FORMPADA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    deposida = models.DecimalField(db_column='DEPOSIDA', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    fhadepda = models.DateField(db_column='FHADEPDA', blank=True, null=True)  # Field name made lowercase.
    numperda = models.SmallIntegerField(db_column='NUMPERDA', blank=True, null=True)  # Field name made lowercase.
    tarifada = models.DecimalField(db_column='TARIFADA', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    numcuada = models.SmallIntegerField(db_column='NUMCUADA', blank=True, null=True)  # Field name made lowercase.
    fechresda = models.DateField(db_column='FECHRESDA', blank=True, null=True)  # Field name made lowercase.
    cargo1da = models.DecimalField(db_column='CARGO1DA', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    nomcar1da = models.CharField(db_column='NOMCAR1DA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    cuencar1da = models.IntegerField(db_column='CUENCAR1DA', blank=True, null=True)  # Field name made lowercase.
    cargo2da = models.DecimalField(db_column='CARGO2DA', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    nomcar2da = models.CharField(db_column='NOMCAR2DA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    cuencar2da = models.IntegerField(db_column='CUENCAR2DA', blank=True, null=True)  # Field name made lowercase.
    cargo3da = models.DecimalField(db_column='CARGO3DA', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    nomcar3da = models.CharField(db_column='NOMCAR3DA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    cuencar3da = models.IntegerField(db_column='CUENCAR3DA', blank=True, null=True)  # Field name made lowercase.
    cargo4da = models.DecimalField(db_column='CARGO4DA', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    nomcar4da = models.CharField(db_column='NOMCAR4DA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    cuencar4da = models.IntegerField(db_column='CUENCAR4DA', blank=True, null=True)  # Field name made lowercase.
    cargo5da = models.DecimalField(db_column='CARGO5DA', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    nomcar5da = models.CharField(db_column='NOMCAR5DA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    cuencar5da = models.IntegerField(db_column='CUENCAR5DA', blank=True, null=True)  # Field name made lowercase.
    paseda = models.CharField(db_column='PASEDA', max_length=4, blank=True, null=True)  # Field name made lowercase.
    cupagda = models.CharField(db_column='CUPAGDA', max_length=1, blank=True, null=True)  # Field name made lowercase.
    facturada = models.IntegerField(db_column='FACTURADA', blank=True, null=True)  # Field name made lowercase.
    fharesda = models.DateField(db_column='FHARESDA', blank=True, null=True)  # Field name made lowercase.
    nomfol1da = models.CharField(db_column='NOMFOL1DA', max_length=30, blank=True, null=True)  # Field name made lowercase.
    nomfol2da = models.CharField(db_column='NOMFOL2DA', max_length=30, blank=True, null=True)  # Field name made lowercase.
    nomfol3da = models.CharField(db_column='NOMFOL3DA', max_length=30, blank=True, null=True)  # Field name made lowercase.
    folex1da = models.CharField(db_column='FOLEX1DA', max_length=3, blank=True, null=True)  # Field name made lowercase.
    folex2da = models.CharField(db_column='FOLEX2DA', max_length=3, blank=True, null=True)  # Field name made lowercase.
    folex3da = models.CharField(db_column='FOLEX3DA', max_length=3, blank=True, null=True)  # Field name made lowercase.
    folex4da = models.CharField(db_column='FOLEX4DA', max_length=3, blank=True, null=True)  # Field name made lowercase.
    folex5da = models.CharField(db_column='FOLEX5DA', max_length=3, blank=True, null=True)  # Field name made lowercase.
    folex6da = models.CharField(db_column='FOLEX6DA', max_length=3, blank=True, null=True)  # Field name made lowercase.
    auditda = models.CharField(db_column='AUDITDA', max_length=1, blank=True, null=True)  # Field name made lowercase.
    horasalida = models.CharField(db_column='HORASALIDA', max_length=5, blank=True, null=True)  # Field name made lowercase.
    horaentrad = models.CharField(db_column='HORAENTRAD', max_length=5, blank=True, null=True)  # Field name made lowercase.
    progeoda = models.CharField(db_column='PROGEODA', max_length=4, blank=True, null=True)  # Field name made lowercase.
    cvegrlda = models.CharField(db_column='CVEGRLDA', max_length=5, blank=True, null=True)  # Field name made lowercase.
    numhuesda = models.IntegerField(db_column='NUMHUESDA', blank=True, null=True)  # Field name made lowercase.
    tpocam1da = models.IntegerField(db_column='TPOCAM1DA', blank=True, null=True)  # Field name made lowercase.
    tpocam2da = models.IntegerField(db_column='TPOCAM2DA', blank=True, null=True)  # Field name made lowercase.
    tpocam3da = models.IntegerField(db_column='TPOCAM3DA', blank=True, null=True)  # Field name made lowercase.
    tpocam4da = models.IntegerField(db_column='TPOCAM4DA', blank=True, null=True)  # Field name made lowercase.
    tpocam5da = models.IntegerField(db_column='TPOCAM5DA', blank=True, null=True)  # Field name made lowercase.
    tpocam6da = models.IntegerField(db_column='TPOCAM6DA', blank=True, null=True)  # Field name made lowercase.
    ctamsrda = models.IntegerField(db_column='CTAMSRDA', blank=True, null=True)  # Field name made lowercase.
    garantiada = models.CharField(db_column='GARANTIADA', max_length=1, blank=True, null=True)  # Field name made lowercase.
    cveestda1 = models.CharField(db_column='CVEESTDA1', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda2 = models.CharField(db_column='CVEESTDA2', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda3 = models.CharField(db_column='CVEESTDA3', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda4 = models.CharField(db_column='CVEESTDA4', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda5 = models.CharField(db_column='CVEESTDA5', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda6 = models.CharField(db_column='CVEESTDA6', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda7 = models.CharField(db_column='CVEESTDA7', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda8 = models.CharField(db_column='CVEESTDA8', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda9 = models.CharField(db_column='CVEESTDA9', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda10 = models.CharField(db_column='CVEESTDA10', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda11 = models.CharField(db_column='CVEESTDA11', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda12 = models.CharField(db_column='CVEESTDA12', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda13 = models.CharField(db_column='CVEESTDA13', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda14 = models.CharField(db_column='CVEESTDA14', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda15 = models.CharField(db_column='CVEESTDA15', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveestda16 = models.CharField(db_column='CVEESTDA16', max_length=5, blank=True, null=True)  # Field name made lowercase.
    habex1da = models.SmallIntegerField(db_column='HABEX1DA', blank=True, null=True)  # Field name made lowercase.
    habex2da = models.SmallIntegerField(db_column='HABEX2DA', blank=True, null=True)  # Field name made lowercase.
    habex3da = models.SmallIntegerField(db_column='HABEX3DA', blank=True, null=True)  # Field name made lowercase.
    habex4da = models.SmallIntegerField(db_column='HABEX4DA', blank=True, null=True)  # Field name made lowercase.
    habex5da = models.SmallIntegerField(db_column='HABEX5DA', blank=True, null=True)  # Field name made lowercase.
    iniregda = models.CharField(db_column='INIREGDA', max_length=5, blank=True, null=True)  # Field name made lowercase.
    horamovda = models.CharField(db_column='HORAMOVDA', max_length=5, blank=True, null=True)  # Field name made lowercase.
    creditoda = models.CharField(db_column='CREDITODA', max_length=1, blank=True, null=True)  # Field name made lowercase.
    numninda = models.SmallIntegerField(db_column='NUMNINDA', blank=True, null=True)  # Field name made lowercase.
    cuentasda = models.CharField(db_column='CUENTASDA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    rfcda = models.CharField(db_column='RFCDA', max_length=20, blank=True, null=True)  # Field name made lowercase.
    dirciada = models.CharField(db_column='DIRCIADA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    ciudciada = models.CharField(db_column='CIUDCIADA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    factada = models.CharField(db_column='FACTADA', max_length=1, blank=True, null=True)  # Field name made lowercase.
    forresda = models.DateField(db_column='FORRESDA', blank=True, null=True)  # Field name made lowercase.
    fhalimda = models.DateField(db_column='FHALIMDA', blank=True, null=True)  # Field name made lowercase.
    horlimda = models.CharField(db_column='HORLIMDA', max_length=8, blank=True, null=True)  # Field name made lowercase.
    tiptarda = models.CharField(db_column='TIPTARDA', max_length=1, blank=True, null=True)  # Field name made lowercase.
    cvegpoda = models.CharField(db_column='CVEGPODA', max_length=8, blank=True, null=True)  # Field name made lowercase.
    cajaseda = models.CharField(db_column='CAJASEDA', max_length=4, blank=True, null=True)  # Field name made lowercase.
    numregda = models.IntegerField(db_column='NUMREGDA', blank=True, null=True)  # Field name made lowercase.
    origenda = models.CharField(db_column='ORIGENDA', max_length=8, blank=True, null=True)  # Field name made lowercase.
    estresda = models.SmallIntegerField(db_column='ESTRESDA', blank=True, null=True)  # Field name made lowercase.
    numvipda = models.IntegerField(db_column='NUMVIPDA', blank=True, null=True)  # Field name made lowercase.
    numextda = models.IntegerField(db_column='NUMEXTDA', blank=True, null=True)  # Field name made lowercase.
    numcanda = models.IntegerField(db_column='NUMCANDA', blank=True, null=True)  # Field name made lowercase.
    canregda = models.IntegerField(db_column='CANREGDA', blank=True, null=True)  # Field name made lowercase.
    fharegda = models.DateField(db_column='FHAREGDA', blank=True, null=True)  # Field name made lowercase.
    estcalda = models.IntegerField(db_column='ESTCALDA', blank=True, null=True)  # Field name made lowercase.
    moncreda = models.IntegerField(db_column='MONCREDA', blank=True, null=True)  # Field name made lowercase.
    mascreda = models.CharField(db_column='MASCREDA', max_length=5, blank=True, null=True)  # Field name made lowercase.
    tarninda = models.IntegerField(db_column='TARNINDA', blank=True, null=True)  # Field name made lowercase.
    estadoda = models.CharField(db_column='ESTADODA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    numlisda = models.IntegerField(db_column='NUMLISDA', blank=True, null=True)  # Field name made lowercase.
    edad01da = models.IntegerField(db_column='EDAD01DA', blank=True, null=True)  # Field name made lowercase.
    edad02da = models.IntegerField(db_column='EDAD02DA', blank=True, null=True)  # Field name made lowercase.
    edad03da = models.IntegerField(db_column='EDAD03DA', blank=True, null=True)  # Field name made lowercase.
    edad04da = models.IntegerField(db_column='EDAD04DA', blank=True, null=True)  # Field name made lowercase.
    edad05da = models.IntegerField(db_column='EDAD05DA', blank=True, null=True)  # Field name made lowercase.
    edad06da = models.IntegerField(db_column='EDAD06DA', blank=True, null=True)  # Field name made lowercase.
    edad07da = models.IntegerField(db_column='EDAD07DA', blank=True, null=True)  # Field name made lowercase.
    edad08da = models.IntegerField(db_column='EDAD08DA', blank=True, null=True)  # Field name made lowercase.
    edad09da = models.IntegerField(db_column='EDAD09DA', blank=True, null=True)  # Field name made lowercase.
    folcedda = models.CharField(db_column='FOLCEDDA', max_length=20, blank=True, null=True)  # Field name made lowercase.
    nombr_da = models.CharField(db_column='NOMBR_DA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    appat_da = models.CharField(db_column='APPAT_DA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    apmat_da = models.CharField(db_column='APMAT_DA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    email_da = models.CharField(db_column='EMAIL_DA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    codposda = models.CharField(db_column='CODPOSDA', max_length=20, blank=True, null=True)  # Field name made lowercase.
    telefoda = models.CharField(db_column='TELEFODA', max_length=60, blank=True, null=True)  # Field name made lowercase.
    porcomda = models.DecimalField(db_column='PORCOMDA', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    tipcomda = models.CharField(db_column='TIPCOMDA', max_length=1, blank=True, null=True)  # Field name made lowercase.
    ticamoda = models.DecimalField(db_column='TICAMODA', max_digits=15, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    iniresda = models.CharField(db_column='INIRESDA', max_length=4, blank=True, null=True)  # Field name made lowercase.
    horaresda = models.CharField(db_column='HORARESDA', max_length=5, blank=True, null=True)  # Field name made lowercase.
    tartarifar = models.DecimalField(db_column='TARTARIFAR', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    motivoforz = models.CharField(db_column='MOTIVOFORZ', max_length=100, blank=True, null=True)  # Field name made lowercase.
    observda4 = models.CharField(db_column='OBSERVDA4', max_length=100, blank=True, null=True)  # Field name made lowercase.
    observda5 = models.CharField(db_column='OBSERVDA5', max_length=100, blank=True, null=True)  # Field name made lowercase.
    nino_free = models.IntegerField(db_column='NINO_FREE', blank=True, null=True)  # Field name made lowercase.
    infantes = models.IntegerField(db_column='INFANTES', blank=True, null=True)  # Field name made lowercase.
    cupon = models.CharField(db_column='CUPON', max_length=30, blank=True, null=True)  # Field name made lowercase.
    numlis2 = models.IntegerField(db_column='NUMLIS2', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DB_DATOS'


class DbFhacie(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    cliente = models.CharField(db_column='CLIENTE', max_length=100, blank=True, null=True)  # Field name made lowercase.
    fha_inicia = models.DateField(db_column='FHA_INICIA', blank=True, null=True)  # Field name made lowercase.
    fha_final = models.DateField(db_column='FHA_FINAL', blank=True, null=True)  # Field name made lowercase.
    fha_capt = models.DateField(db_column='FHA_CAPT', blank=True, null=True)  # Field name made lowercase.
    estatus = models.CharField(db_column='ESTATUS', max_length=1, blank=True, null=True)  # Field name made lowercase.
    reabierto = models.CharField(db_column='REABIERTO', max_length=1, blank=True, null=True)  # Field name made lowercase.
    usu_cierra = models.CharField(db_column='USU_CIERRA', max_length=4, blank=True, null=True)  # Field name made lowercase.
    usu_abre = models.CharField(db_column='USU_ABRE', max_length=4, blank=True, null=True)  # Field name made lowercase.
    fha_abre = models.DateField(db_column='FHA_ABRE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DB_FHACIE'


class DbOcup(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    clave = models.CharField(db_column='CLAVE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    fecha = models.DateField(db_column='FECHA', blank=True, null=True)  # Field name made lowercase.
    inic = models.SmallIntegerField(db_column='INIC', blank=True, null=True)  # Field name made lowercase.
    entr = models.SmallIntegerField(db_column='ENTR', blank=True, null=True)  # Field name made lowercase.
    sali = models.SmallIntegerField(db_column='SALI', blank=True, null=True)  # Field name made lowercase.
    bloc = models.SmallIntegerField(db_column='BLOC', blank=True, null=True)  # Field name made lowercase.
    cier = models.SmallIntegerField(db_column='CIER', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DB_OCUP'


class FeDetCg(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    folio = models.IntegerField(db_column='FOLIO', blank=True, null=True)  # Field name made lowercase.
    cuenta = models.IntegerField(db_column='CUENTA', blank=True, null=True)  # Field name made lowercase.
    desc_cta = models.CharField(db_column='DESC_CTA', max_length=150, blank=True, null=True)  # Field name made lowercase.
    cantidad = models.DecimalField(db_column='CANTIDAD', max_digits=8, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    punitario = models.DecimalField(db_column='PUNITARIO', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    total = models.DecimalField(db_column='TOTAL', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    fecha = models.DateField(db_column='FECHA', blank=True, null=True)  # Field name made lowercase.
    usuario = models.CharField(db_column='USUARIO', max_length=4, blank=True, null=True)  # Field name made lowercase.
    estatus = models.SmallIntegerField(db_column='ESTATUS', blank=True, null=True)  # Field name made lowercase.
    num_hue = models.IntegerField(db_column='NUM_HUE', blank=True, null=True)  # Field name made lowercase.
    cuarto = models.SmallIntegerField(db_column='CUARTO', blank=True, null=True)  # Field name made lowercase.
    cargo = models.DecimalField(db_column='CARGO', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    credito = models.DecimalField(db_column='CREDITO', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    factura = models.IntegerField(db_column='FACTURA', blank=True, null=True)  # Field name made lowercase.
    sefactura = models.CharField(db_column='SEFACTURA', max_length=1, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'FE_DET_CG'


class FeEncCg(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    folio = models.IntegerField(db_column='FOLIO', blank=True, null=True)  # Field name made lowercase.
    cuarto = models.SmallIntegerField(db_column='CUARTO', blank=True, null=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=120, blank=True, null=True)  # Field name made lowercase.
    razonsoc = models.CharField(db_column='RAZONSOC', max_length=150, blank=True, null=True)  # Field name made lowercase.
    rfc = models.CharField(db_column='RFC', max_length=20, blank=True, null=True)  # Field name made lowercase.
    direccion = models.CharField(db_column='DIRECCION', max_length=100, blank=True, null=True)  # Field name made lowercase.
    calle = models.CharField(db_column='CALLE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    noexterior = models.CharField(db_column='NOEXTERIOR', max_length=10, blank=True, null=True)  # Field name made lowercase.
    nointerior = models.CharField(db_column='NOINTERIOR', max_length=10, blank=True, null=True)  # Field name made lowercase.
    colonia = models.CharField(db_column='COLONIA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ciudad = models.CharField(db_column='CIUDAD', max_length=50, blank=True, null=True)  # Field name made lowercase.
    municipio = models.CharField(db_column='MUNICIPIO', max_length=50, blank=True, null=True)  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=50, blank=True, null=True)  # Field name made lowercase.
    pais = models.CharField(db_column='PAIS', max_length=10, blank=True, null=True)  # Field name made lowercase.
    codpostal = models.CharField(db_column='CODPOSTAL', max_length=10, blank=True, null=True)  # Field name made lowercase.
    subtotal = models.DecimalField(db_column='SUBTOTAL', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    iva = models.DecimalField(db_column='IVA', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    hosp = models.DecimalField(db_column='HOSP', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    exento = models.DecimalField(db_column='EXENTO', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    total = models.DecimalField(db_column='TOTAL', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    num_a_let = models.CharField(db_column='NUM_A_LET', max_length=120, blank=True, null=True)  # Field name made lowercase.
    fhaentrada = models.DateField(db_column='FHAENTRADA', blank=True, null=True)  # Field name made lowercase.
    fhacout = models.DateField(db_column='FHACOUT', blank=True, null=True)  # Field name made lowercase.
    num_hue = models.IntegerField(db_column='NUM_HUE', blank=True, null=True)  # Field name made lowercase.
    estatus = models.CharField(db_column='ESTATUS', max_length=1, blank=True, null=True)  # Field name made lowercase.
    hora = models.CharField(db_column='HORA', max_length=10, blank=True, null=True)  # Field name made lowercase.
    seriefact = models.CharField(db_column='SERIEFACT', max_length=10, blank=True, null=True)  # Field name made lowercase.
    factura = models.IntegerField(db_column='FACTURA', blank=True, null=True)  # Field name made lowercase.
    fhafactura = models.DateField(db_column='FHAFACTURA', blank=True, null=True)  # Field name made lowercase.
    cadenaorig = models.TextField(db_column='CADENAORIG', blank=True, null=True)  # Field name made lowercase.
    sellodigit = models.TextField(db_column='SELLODIGIT', blank=True, null=True)  # Field name made lowercase.
    usuario = models.CharField(db_column='USUARIO', max_length=4, blank=True, null=True)  # Field name made lowercase.
    revision = models.CharField(db_column='REVISION', max_length=1, blank=True, null=True)  # Field name made lowercase.
    cveage = models.CharField(db_column='CVEAGE', max_length=4, blank=True, null=True)  # Field name made lowercase.
    cvecia = models.CharField(db_column='CVECIA', max_length=4, blank=True, null=True)  # Field name made lowercase.
    cupon = models.CharField(db_column='CUPON', max_length=30, blank=True, null=True)  # Field name made lowercase.
    tarifamon = models.SmallIntegerField(db_column='TARIFAMON', blank=True, null=True)  # Field name made lowercase.
    numreserva = models.IntegerField(db_column='NUMRESERVA', blank=True, null=True)  # Field name made lowercase.
    selfac = models.TextField(db_column='SELFAC', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    facturadox = models.CharField(db_column='FACTURADOX', max_length=4, blank=True, null=True)  # Field name made lowercase.
    est_folio = models.CharField(db_column='EST_FOLIO', max_length=60, blank=True, null=True)  # Field name made lowercase.
    sefactura = models.CharField(db_column='SEFACTURA', max_length=1, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'FE_ENC_CG'


class Infext07(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    numhue07 = models.IntegerField(db_column='NUMHUE07', blank=True, null=True)  # Field name made lowercase.
    numres07 = models.IntegerField(db_column='NUMRES07', blank=True, null=True)  # Field name made lowercase.
    puntaj07 = models.DecimalField(db_column='PUNTAJ07', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    status07 = models.CharField(db_column='STATUS07', max_length=1, blank=True, null=True)  # Field name made lowercase.
    camp0107 = models.CharField(db_column='CAMP0107', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp0207 = models.CharField(db_column='CAMP0207', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp0307 = models.CharField(db_column='CAMP0307', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp0407 = models.CharField(db_column='CAMP0407', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp0507 = models.CharField(db_column='CAMP0507', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp0607 = models.CharField(db_column='CAMP0607', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp0707 = models.CharField(db_column='CAMP0707', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp0807 = models.CharField(db_column='CAMP0807', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp0907 = models.CharField(db_column='CAMP0907', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp1007 = models.CharField(db_column='CAMP1007', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp1107 = models.CharField(db_column='CAMP1107', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp1207 = models.CharField(db_column='CAMP1207', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp1307 = models.CharField(db_column='CAMP1307', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp1407 = models.CharField(db_column='CAMP1407', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp1507 = models.CharField(db_column='CAMP1507', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp1607 = models.CharField(db_column='CAMP1607', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp1707 = models.CharField(db_column='CAMP1707', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp1807 = models.CharField(db_column='CAMP1807', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp1907 = models.CharField(db_column='CAMP1907', max_length=70, blank=True, null=True)  # Field name made lowercase.
    camp2007 = models.CharField(db_column='CAMP2007', max_length=70, blank=True, null=True)  # Field name made lowercase.
    cveee0107 = models.CharField(db_column='CVEEE0107', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee0207 = models.CharField(db_column='CVEEE0207', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee0307 = models.CharField(db_column='CVEEE0307', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee0407 = models.CharField(db_column='CVEEE0407', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee0507 = models.CharField(db_column='CVEEE0507', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee0607 = models.CharField(db_column='CVEEE0607', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee0707 = models.CharField(db_column='CVEEE0707', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee0807 = models.CharField(db_column='CVEEE0807', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee0907 = models.CharField(db_column='CVEEE0907', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee1007 = models.CharField(db_column='CVEEE1007', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee1107 = models.CharField(db_column='CVEEE1107', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee1207 = models.CharField(db_column='CVEEE1207', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee1307 = models.CharField(db_column='CVEEE1307', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee1407 = models.CharField(db_column='CVEEE1407', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee1507 = models.CharField(db_column='CVEEE1507', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cveee1607 = models.CharField(db_column='CVEEE1607', max_length=5, blank=True, null=True)  # Field name made lowercase.
    strvip07 = models.CharField(db_column='STRVIP07', max_length=6, blank=True, null=True)  # Field name made lowercase.
    catvip07 = models.CharField(db_column='CATVIP07', max_length=1, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'INFEXT07'


class Relcar34(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    status34 = models.CharField(db_column='STATUS34', max_length=1, blank=True, null=True)  # Field name made lowercase.
    numhue34 = models.IntegerField(db_column='NUMHUE34', blank=True, null=True)  # Field name made lowercase.
    numres34 = models.IntegerField(db_column='NUMRES34', blank=True, null=True)  # Field name made lowercase.
    numcar34 = models.IntegerField(db_column='NUMCAR34', blank=True, null=True)  # Field name made lowercase.
    forcar34 = models.CharField(db_column='FORCAR34', max_length=1, blank=True, null=True)  # Field name made lowercase.
    numdia34 = models.IntegerField(db_column='NUMDIA34', blank=True, null=True)  # Field name made lowercase.
    fhaesp34 = models.DateField(db_column='FHAESP34', blank=True, null=True)  # Field name made lowercase.
    porcom34 = models.DecimalField(db_column='PORCOM34', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    tipcom34 = models.CharField(db_column='TIPCOM34', max_length=1, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RELCAR34'


class Tabconf(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    cvetab = models.CharField(db_column='CVETAB', max_length=1, blank=True, null=True)  # Field name made lowercase.
    nomtab = models.CharField(db_column='NOMTAB', max_length=30, blank=True, null=True)  # Field name made lowercase.
    tipo = models.CharField(db_column='TIPO', max_length=1, blank=True, null=True)  # Field name made lowercase.
    nomcor = models.CharField(db_column='NOMCOR', max_length=4, blank=True, null=True)  # Field name made lowercase.
    grupal = models.CharField(db_column='GRUPAL', max_length=1, blank=True, null=True)  # Field name made lowercase.
    campo1 = models.CharField(db_column='CAMPO1', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo2 = models.CharField(db_column='CAMPO2', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo3 = models.CharField(db_column='CAMPO3', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo4 = models.CharField(db_column='CAMPO4', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo5 = models.CharField(db_column='CAMPO5', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo6 = models.CharField(db_column='CAMPO6', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo7 = models.CharField(db_column='CAMPO7', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo8 = models.CharField(db_column='CAMPO8', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo9 = models.CharField(db_column='CAMPO9', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo10 = models.CharField(db_column='CAMPO10', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo11 = models.CharField(db_column='CAMPO11', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo12 = models.CharField(db_column='CAMPO12', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo13 = models.CharField(db_column='CAMPO13', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo14 = models.CharField(db_column='CAMPO14', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo15 = models.CharField(db_column='CAMPO15', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo16 = models.CharField(db_column='CAMPO16', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo17 = models.CharField(db_column='CAMPO17', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo18 = models.CharField(db_column='CAMPO18', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo19 = models.CharField(db_column='CAMPO19', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo20 = models.CharField(db_column='CAMPO20', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo21 = models.CharField(db_column='CAMPO21', max_length=20, blank=True, null=True)  # Field name made lowercase.
    campo22 = models.CharField(db_column='CAMPO22', max_length=20, blank=True, null=True)  # Field name made lowercase.
    cveest1 = models.CharField(db_column='CVEEST1', max_length=1, blank=True, null=True)  # Field name made lowercase.
    cveest2 = models.CharField(db_column='CVEEST2', max_length=1, blank=True, null=True)  # Field name made lowercase.
    cveest3 = models.CharField(db_column='CVEEST3', max_length=1, blank=True, null=True)  # Field name made lowercase.
    cveest4 = models.CharField(db_column='CVEEST4', max_length=1, blank=True, null=True)  # Field name made lowercase.
    cveesta2 = models.CharField(db_column='CVEESTA2', max_length=4, blank=True, null=True)  # Field name made lowercase.
    nomtab2 = models.CharField(db_column='NOMTAB2', max_length=30, blank=True, null=True)  # Field name made lowercase.
    cvepspg = models.CharField(db_column='CVEPSPG', max_length=1, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TABCONF'


class TabEst(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    clave = models.CharField(db_column='CLAVE', max_length=4, blank=True, null=True)  # Field name made lowercase.
    descrip = models.CharField(db_column='DESCRIP', max_length=100, blank=True, null=True)  # Field name made lowercase.
    otro1 = models.CharField(db_column='OTRO1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    otro2 = models.CharField(db_column='OTRO2', max_length=50, blank=True, null=True)  # Field name made lowercase.
    otro3 = models.CharField(db_column='OTRO3', max_length=45, blank=True, null=True)  # Field name made lowercase.
    otro4 = models.DecimalField(db_column='OTRO4', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    otro5 = models.DecimalField(db_column='OTRO5', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    otro6 = models.DecimalField(db_column='OTRO6', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    otro7 = models.CharField(db_column='OTRO7', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro8 = models.CharField(db_column='OTRO8', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro9 = models.CharField(db_column='OTRO9', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro10 = models.CharField(db_column='OTRO10', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro11 = models.CharField(db_column='OTRO11', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro12 = models.CharField(db_column='OTRO12', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro13 = models.CharField(db_column='OTRO13', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro14 = models.CharField(db_column='OTRO14', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro15 = models.CharField(db_column='OTRO15', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro16 = models.CharField(db_column='OTRO16', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro17 = models.CharField(db_column='OTRO17', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro18 = models.CharField(db_column='OTRO18', max_length=55, blank=True, null=True)  # Field name made lowercase.
    otro19 = models.CharField(db_column='OTRO19', max_length=4, blank=True, null=True)  # Field name made lowercase.
    otro20 = models.CharField(db_column='OTRO20', max_length=4, blank=True, null=True)  # Field name made lowercase.
    otro21 = models.CharField(db_column='OTRO21', max_length=4, blank=True, null=True)  # Field name made lowercase.
    otro22 = models.CharField(db_column='OTRO22', max_length=4, blank=True, null=True)  # Field name made lowercase.
    typtab = models.CharField(db_column='TYPTAB', max_length=1, blank=True, null=True)  # Field name made lowercase.
    ctosdia = models.SmallIntegerField(db_column='CTOSDIA', blank=True, null=True)  # Field name made lowercase.
    ctosprodia = models.SmallIntegerField(db_column='CTOSPRODIA', blank=True, null=True)  # Field name made lowercase.
    tarifadia = models.DecimalField(db_column='TARIFADIA', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    entradia = models.SmallIntegerField(db_column='ENTRADIA', blank=True, null=True)  # Field name made lowercase.
    noperdia = models.SmallIntegerField(db_column='NOPERDIA', blank=True, null=True)  # Field name made lowercase.
    ctosmes = models.IntegerField(db_column='CTOSMES', blank=True, null=True)  # Field name made lowercase.
    ctospromes = models.IntegerField(db_column='CTOSPROMES', blank=True, null=True)  # Field name made lowercase.
    nopermes = models.IntegerField(db_column='NOPERMES', blank=True, null=True)  # Field name made lowercase.
    tarifames = models.DecimalField(db_column='TARIFAMES', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    entrames = models.SmallIntegerField(db_column='ENTRAMES', blank=True, null=True)  # Field name made lowercase.
    ctosan = models.IntegerField(db_column='CTOSAN', blank=True, null=True)  # Field name made lowercase.
    ctosproan = models.IntegerField(db_column='CTOSPROAN', blank=True, null=True)  # Field name made lowercase.
    noperan = models.IntegerField(db_column='NOPERAN', blank=True, null=True)  # Field name made lowercase.
    tarifaan = models.DecimalField(db_column='TARIFAAN', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    entraan = models.IntegerField(db_column='ENTRAAN', blank=True, null=True)  # Field name made lowercase.
    grupal = models.CharField(db_column='GRUPAL', max_length=20, blank=True, null=True)  # Field name made lowercase.
    defcap = models.SmallIntegerField(db_column='DEFCAP', blank=True, null=True)  # Field name made lowercase.
    clasif = models.CharField(db_column='CLASIF', max_length=20, blank=True, null=True)  # Field name made lowercase.
    vipant = models.IntegerField(db_column='VIPANT', blank=True, null=True)  # Field name made lowercase.
    vipact = models.IntegerField(db_column='VIPACT', blank=True, null=True)  # Field name made lowercase.
    concredito = models.IntegerField(db_column='CONCREDITO', blank=True, null=True)  # Field name made lowercase.
    segmento = models.CharField(db_column='SEGMENTO', max_length=20, blank=True, null=True)  # Field name made lowercase.
    origengeog = models.CharField(db_column='ORIGENGEOG', max_length=4, blank=True, null=True)  # Field name made lowercase.
    cta_cadmin = models.IntegerField(db_column='cTA_CADMIN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TAB_EST'


class TabHt(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    siufp = models.DateField(db_column='SIUFP', blank=True, null=True)  # Field name made lowercase.
    ultpase = models.CharField(db_column='ULTPASE', max_length=4, blank=True, null=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=100, blank=True, null=True)  # Field name made lowercase.
    direccion = models.CharField(db_column='DIRECCION', max_length=100, blank=True, null=True)  # Field name made lowercase.
    tel1 = models.CharField(db_column='TEL1', max_length=20, blank=True, null=True)  # Field name made lowercase.
    tel2 = models.CharField(db_column='TEL2', max_length=20, blank=True, null=True)  # Field name made lowercase.
    tel3 = models.CharField(db_column='TEL3', max_length=20, blank=True, null=True)  # Field name made lowercase.
    ciudad = models.CharField(db_column='CIUDAD', max_length=50, blank=True, null=True)  # Field name made lowercase.
    pais = models.CharField(db_column='PAIS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    no_res = models.IntegerField(db_column='NO_RES', blank=True, null=True)  # Field name made lowercase.
    no_blok = models.IntegerField(db_column='NO_BLOK', blank=True, null=True)  # Field name made lowercase.
    no_folio = models.IntegerField(db_column='NO_FOLIO', blank=True, null=True)  # Field name made lowercase.
    no_factura = models.IntegerField(db_column='NO_FACTURA', blank=True, null=True)  # Field name made lowercase.
    no_ctadep = models.IntegerField(db_column='NO_CTADEP', blank=True, null=True)  # Field name made lowercase.
    no_tarifa = models.IntegerField(db_column='NO_TARIFA', blank=True, null=True)  # Field name made lowercase.
    grupoac = models.IntegerField(db_column='GRUPOAC', blank=True, null=True)  # Field name made lowercase.
    grupoini = models.IntegerField(db_column='GRUPOINI', blank=True, null=True)  # Field name made lowercase.
    fhafect = models.DateField(db_column='FHAFECT', blank=True, null=True)  # Field name made lowercase.
    fhaci = models.DateField(db_column='FHACI', blank=True, null=True)  # Field name made lowercase.
    fha100 = models.DateField(db_column='FHA100', blank=True, null=True)  # Field name made lowercase.
    no_ctaiva = models.IntegerField(db_column='NO_CTAIVA', blank=True, null=True)  # Field name made lowercase.
    no_resapl = models.IntegerField(db_column='NO_RESAPL', blank=True, null=True)  # Field name made lowercase.
    no_resapc = models.IntegerField(db_column='NO_RESAPC', blank=True, null=True)  # Field name made lowercase.
    poriva = models.DecimalField(db_column='PORIVA', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    razsoc = models.CharField(db_column='RAZSOC', max_length=40, blank=True, null=True)  # Field name made lowercase.
    rfc = models.CharField(db_column='RFC', max_length=15, blank=True, null=True)  # Field name made lowercase.
    ctiva = models.CharField(db_column='CTIVA', max_length=15, blank=True, null=True)  # Field name made lowercase.
    canaco = models.CharField(db_column='CANACO', max_length=15, blank=True, null=True)  # Field name made lowercase.
    telex = models.CharField(db_column='TELEX', max_length=20, blank=True, null=True)  # Field name made lowercase.
    obs1 = models.CharField(db_column='OBS1', max_length=50, blank=True, null=True)  # Field name made lowercase.
    obs2 = models.CharField(db_column='OBS2', max_length=50, blank=True, null=True)  # Field name made lowercase.
    obs3 = models.CharField(db_column='OBS3', max_length=50, blank=True, null=True)  # Field name made lowercase.
    balance = models.IntegerField(db_column='BALANCE', blank=True, null=True)  # Field name made lowercase.
    fact = models.CharField(db_column='FACT', max_length=1, blank=True, null=True)  # Field name made lowercase.
    numhuesped = models.IntegerField(db_column='NUMHUESPED', blank=True, null=True)  # Field name made lowercase.
    cta_traspg = models.IntegerField(db_column='CTA_TRASPG', blank=True, null=True)  # Field name made lowercase.
    cta_traspe = models.IntegerField(db_column='CTA_TRASPE', blank=True, null=True)  # Field name made lowercase.
    cta_traspp = models.IntegerField(db_column='CTA_TRASPP', blank=True, null=True)  # Field name made lowercase.
    cta_traspi = models.IntegerField(db_column='CTA_TRASPI', blank=True, null=True)  # Field name made lowercase.
    cta_traspa = models.IntegerField(db_column='CTA_TRASPA', blank=True, null=True)  # Field name made lowercase.
    porhosp = models.DecimalField(db_column='PORHOSP', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    no_ctahosp = models.IntegerField(db_column='NO_CTAHOSP', blank=True, null=True)  # Field name made lowercase.
    cta_trasho = models.IntegerField(db_column='CTA_TRASHO', blank=True, null=True)  # Field name made lowercase.
    no_cancel = models.IntegerField(db_column='NO_CANCEL', blank=True, null=True)  # Field name made lowercase.
    balance2 = models.IntegerField(db_column='BALANCE2', blank=True, null=True)  # Field name made lowercase.
    depbal2 = models.CharField(db_column='DEPBAL2', max_length=3, blank=True, null=True)  # Field name made lowercase.
    no_usa = models.IntegerField(db_column='NO_USA', blank=True, null=True)  # Field name made lowercase.
    no_res02 = models.IntegerField(db_column='NO_RES02', blank=True, null=True)  # Field name made lowercase.
    no_can02 = models.IntegerField(db_column='NO_CAN02', blank=True, null=True)  # Field name made lowercase.
    ctahab02 = models.IntegerField(db_column='CTAHAB02', blank=True, null=True)  # Field name made lowercase.
    ctaiva02 = models.IntegerField(db_column='CTAIVA02', blank=True, null=True)  # Field name made lowercase.
    ctahos02 = models.IntegerField(db_column='CTAHOS02', blank=True, null=True)  # Field name made lowercase.
    folpaq02 = models.IntegerField(db_column='FOLPAQ02', blank=True, null=True)  # Field name made lowercase.
    cta_trassu = models.IntegerField(db_column='CTA_TRASSU', blank=True, null=True)  # Field name made lowercase.
    cta_trasha = models.IntegerField(db_column='CTA_TRASHA', blank=True, null=True)  # Field name made lowercase.
    cta_traspq = models.IntegerField(db_column='CTA_TRASPQ', blank=True, null=True)  # Field name made lowercase.
    no_usuar = models.IntegerField(db_column='NO_USUAR', blank=True, null=True)  # Field name made lowercase.
    conscuni = models.IntegerField(db_column='CONSCUNI', blank=True, null=True)  # Field name made lowercase.
    exe_cxc = models.CharField(db_column='EXE_CXC', max_length=40, blank=True, null=True)  # Field name made lowercase.
    exe_ing1 = models.CharField(db_column='EXE_ING1', max_length=40, blank=True, null=True)  # Field name made lowercase.
    exe_ing2 = models.CharField(db_column='EXE_ING2', max_length=40, blank=True, null=True)  # Field name made lowercase.
    ruta_fact = models.CharField(db_column='RUTA_FACT', max_length=60, blank=True, null=True)  # Field name made lowercase.
    folio_pago = models.IntegerField(db_column='FOLIO_PAGO', blank=True, null=True)  # Field name made lowercase.
    imprimerec = models.CharField(db_column='IMPRIMEREC', max_length=1, blank=True, null=True)  # Field name made lowercase.
    copiasrec = models.IntegerField(db_column='COPIASREC', blank=True, null=True)  # Field name made lowercase.
    cta_muni = models.IntegerField(db_column='CTA_MUNI', blank=True, null=True)  # Field name made lowercase.
    monto_muni = models.DecimalField(db_column='MONTO_MUNI', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    cta_cont_m = models.IntegerField(db_column='CTA_CONT_M', blank=True, null=True)  # Field name made lowercase.
    tar_inc_im = models.CharField(db_column='TAR_INC_IM', max_length=1, blank=True, null=True)  # Field name made lowercase.
    tipo_bd = models.IntegerField(db_column='TIPO_BD', blank=True, null=True)  # Field name made lowercase.
    paqmuni = models.CharField(db_column='PAQMUNI', max_length=4, blank=True, null=True)  # Field name made lowercase.
    cta_ides = models.IntegerField(db_column='CTA_IDES', blank=True, null=True)  # Field name made lowercase.
    porc_ides = models.DecimalField(db_column='PORC_IDES', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    ctac_ides = models.IntegerField(db_column='CTAC_IDES', blank=True, null=True)  # Field name made lowercase.
    t_inc_ides = models.CharField(db_column='T_INC_IDES', max_length=1, blank=True, null=True)  # Field name made lowercase.
    no_res03 = models.IntegerField(db_column='NO_RES03', blank=True, null=True)  # Field name made lowercase.
    no_can03 = models.IntegerField(db_column='NO_CAN03', blank=True, null=True)  # Field name made lowercase.
    segmento_web = models.CharField(db_column='SEGMENTO_WEB', max_length=4, blank=True, null=True)  # Field name made lowercase.
    agencia_web = models.CharField(db_column='AGENCIA_WEB', max_length=4, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TAB_HT'


class TabIh(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    numeroih = models.SmallIntegerField(db_column='NUMEROIH', blank=True, null=True)  # Field name made lowercase.
    tipoih = models.CharField(db_column='TIPOIH', max_length=4, blank=True, null=True)  # Field name made lowercase.
    edifih = models.CharField(db_column='EDIFIH', max_length=20, blank=True, null=True)  # Field name made lowercase.
    orientih = models.CharField(db_column='ORIENTIH', max_length=30, blank=True, null=True)  # Field name made lowercase.
    caracih = models.CharField(db_column='CARACIH', max_length=30, blank=True, null=True)  # Field name made lowercase.
    facilih = models.CharField(db_column='FACILIH', max_length=30, blank=True, null=True)  # Field name made lowercase.
    pisoih = models.IntegerField(db_column='PISOIH', blank=True, null=True)  # Field name made lowercase.
    descrih = models.CharField(db_column='DESCRIH', max_length=30, blank=True, null=True)  # Field name made lowercase.
    statusih = models.CharField(db_column='STATUSIH', max_length=3, blank=True, null=True)  # Field name made lowercase.
    rackih = models.CharField(db_column='RACKIH', max_length=1, blank=True, null=True)  # Field name made lowercase.
    reserih = models.IntegerField(db_column='RESERIH', blank=True, null=True)  # Field name made lowercase.
    id_camih = models.CharField(db_column='ID_CAMIH', max_length=4, blank=True, null=True)  # Field name made lowercase.
    conhabih = models.SmallIntegerField(db_column='CONHABIH', blank=True, null=True)  # Field name made lowercase.
    zonlocih = models.CharField(db_column='ZONLOCIH', max_length=4, blank=True, null=True)  # Field name made lowercase.
    unidadih = models.IntegerField(db_column='UNIDADIH', blank=True, null=True)  # Field name made lowercase.
    id_grupo = models.CharField(db_column='ID_GRUPO', max_length=1, blank=True, null=True)  # Field name made lowercase.
    prea_ocup = models.CharField(db_column='PREA_OCUP', max_length=1, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TAB_IH'


class TabTc(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nombre_tc = models.CharField(db_column='NOMBRE_TC', max_length=20, blank=True, null=True)  # Field name made lowercase.
    clave_tc = models.IntegerField(db_column='CLAVE_TC', blank=True, null=True)  # Field name made lowercase.
    tipo_tc = models.IntegerField(db_column='TIPO_TC', blank=True, null=True)  # Field name made lowercase.
    clasif1 = models.CharField(db_column='CLASIF1', max_length=2, blank=True, null=True)  # Field name made lowercase.
    clasif2 = models.CharField(db_column='CLASIF2', max_length=2, blank=True, null=True)  # Field name made lowercase.
    impuesto = models.DecimalField(db_column='IMPUESTO', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    tipocta = models.CharField(db_column='TIPOCTA', max_length=1, blank=True, null=True)  # Field name made lowercase.
    ctasinf = models.IntegerField(db_column='CTASINF', blank=True, null=True)  # Field name made lowercase.
    deptotc = models.CharField(db_column='DEPTOTC', max_length=3, blank=True, null=True)  # Field name made lowercase.
    porcomtc = models.DecimalField(db_column='PORCOMTC', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    ctacomtc = models.IntegerField(db_column='CTACOMTC', blank=True, null=True)  # Field name made lowercase.
    depcomtc = models.CharField(db_column='DEPCOMTC', max_length=3, blank=True, null=True)  # Field name made lowercase.
    porivatc = models.DecimalField(db_column='PORIVATC', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    ctaivatc = models.IntegerField(db_column='CTAIVATC', blank=True, null=True)  # Field name made lowercase.
    depivatc = models.CharField(db_column='DEPIVATC', max_length=3, blank=True, null=True)  # Field name made lowercase.
    porprotc = models.DecimalField(db_column='PORPROTC', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    ctaprotc = models.IntegerField(db_column='CTAPROTC', blank=True, null=True)  # Field name made lowercase.
    cvetcutc = models.CharField(db_column='CVETCUTC', max_length=4, blank=True, null=True)  # Field name made lowercase.
    cvespgtc = models.CharField(db_column='CVESPGTC', max_length=1, blank=True, null=True)  # Field name made lowercase.
    tipingtc = models.CharField(db_column='TIPINGTC', max_length=1, blank=True, null=True)  # Field name made lowercase.
    alias_tc = models.CharField(db_column='ALIAS_TC', max_length=20, blank=True, null=True)  # Field name made lowercase.
    ctaajutc = models.IntegerField(db_column='CTAAJUTC', blank=True, null=True)  # Field name made lowercase.
    folpaqtc = models.CharField(db_column='FOLPAQTC', max_length=3, blank=True, null=True)  # Field name made lowercase.
    desimptc = models.CharField(db_column='DESIMPTC', max_length=1, blank=True, null=True)  # Field name made lowercase.
    ctaivhtc = models.IntegerField(db_column='CTAIVHTC', blank=True, null=True)  # Field name made lowercase.
    ctahostc = models.IntegerField(db_column='CTAHOSTC', blank=True, null=True)  # Field name made lowercase.
    porivhtc = models.DecimalField(db_column='PORIVHTC', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    porhostc = models.DecimalField(db_column='PORHOSTC', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    ctaimstc = models.IntegerField(db_column='CTAIMSTC', blank=True, null=True)  # Field name made lowercase.
    porimstc = models.DecimalField(db_column='PORIMSTC', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    foldeftc = models.CharField(db_column='FOLDEFTC', max_length=1, blank=True, null=True)  # Field name made lowercase.
    facivatc = models.IntegerField(db_column='FACIVATC', blank=True, null=True)  # Field name made lowercase.
    ptoleatc = models.CharField(db_column='PTOLEATC', max_length=1, blank=True, null=True)  # Field name made lowercase.
    porptotc = models.DecimalField(db_column='PORPTOTC', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    cta_hues = models.IntegerField(db_column='CTA_HUES', blank=True, null=True)  # Field name made lowercase.
    cta_rdi = models.IntegerField(db_column='CTA_RDI', blank=True, null=True)  # Field name made lowercase.
    obligaref = models.CharField(db_column='OBLIGAREF', max_length=1, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TAB_TC'
