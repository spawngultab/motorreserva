from django import forms
from .models import TabEst, DbDatos


class TabEstForm(forms.ModelForm):
	class Meta:
		model = TabEst
		fields = '__all__'
		widgets = {
			'clave': forms.TextInput(attrs={'class': 'form-control'}),
			'descrip': forms.TextInput(attrs={'class': 'form-control'}),
			'otro1': forms.TextInput(attrs={'class': 'form-control'}),
			'otro2': forms.TextInput(attrs={'class': 'form-control'}),
			'otro3': forms.TextInput(attrs={'class': 'form-control'}),
		}


class DbDatosForm(forms.ModelForm):
	class Meta:
		model = DbDatos
		fields = '__all__'
		widgets = {
			'nombreda': forms.TextInput(attrs={'class': 'form-control', 'hidden': 'hidden'}),
			'statusda': forms.TextInput(attrs={'class': 'form-control', 'value': 'R', 'hidden': 'hidden'}),
			'ciudadda': forms.TextInput(attrs={'class': 'form-control treinta', 'placeholder': 'Ciudad'}),
			'fhaentda': forms.TextInput(attrs={'class': 'form-control fecha', 'type': 'date'}),
			'fhasalda': forms.TextInput(attrs={'class': 'form-control fecha', 'type': 'date'}),
			'paisda': forms.TextInput(attrs={'class': 'form-control treinta', 'placeholder': 'País'}),
			'deposida': forms.NumberInput(attrs={'class': 'form-control', 'step': '0.01', 'value': '0.00'}),
			'fhadepda': forms.TextInput(attrs={'class': 'form-control', 'type': 'date'}),
			'numperda': forms.NumberInput(attrs={'class': 'form-control', 'value': '1'}),
			'tpocam1da': forms.Select(attrs={'class': 'form-select'}),
			'cveestda1': forms.Select(attrs={'class': 'form-select'}),
			'estadoda': forms.TextInput(attrs={'class': 'form-control treinta', 'placeholder': 'Estado'}),
			'tarifada': forms.NumberInput(attrs={'class': 'form-control', 'step': '0.01', 'value': '0.00'}),
			'numcuada': forms.NumberInput(attrs={'class': 'form-control cincuenta', 'value': '1'}),
			'fharesda': forms.TextInput(attrs={'class': 'form-control', 'type': 'date', 'hidden': 'hidden'}),
			'iniresda': forms.TextInput(attrs={'class': 'form-control', 'value': 'WEB', 'hidden': 'hidden'}),
			'infantes': forms.NumberInput(attrs={'class': 'form-control', 'value': '0'}),
			'nombr_da': forms.TextInput(attrs={'class': 'form-control nombapell', 'placeholder': 'Nombre'}),
			'appat_da': forms.TextInput(attrs={'class': 'form-control nombapell', 'placeholder': 'Apellido Paterno'}),
			'apmat_da': forms.TextInput(attrs={'class': 'form-control nombapell', 'placeholder': 'Apellido Materno'}),
			'email_da': forms.TextInput(attrs={'class': 'form-control'}),
			'telefoda': forms.TextInput(attrs={'class': 'form-control'}),
		}
