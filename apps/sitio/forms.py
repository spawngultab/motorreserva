from django import forms
from .models import Sitio


class SitioForm(forms.ModelForm):
	class Meta:
		model = Sitio
		fields = '__all__'
		widgets = {
			'nombre_configuracion': forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}),
			'logo': forms.ClearableFileInput(attrs={'class': 'form-control'}),
			'color_fondo': forms.TextInput(attrs={'type': 'color', 'class': 'form-control'}),
			'color_fondo_sidebar': forms.TextInput(attrs={'type': 'color', 'class': 'form-control'}),
			'color_fondo_navbar': forms.TextInput(attrs={'type': 'color', 'class': 'form-control'}),
			'color_fondo_footer': forms.TextInput(attrs={'type': 'color', 'class': 'form-control'}),
			'idioma': forms.Select(attrs={'class': 'form-select', 'value': 'ESP'}),
			'status': forms.CheckboxInput(attrs={'class': 'form-check-input'})
		}
