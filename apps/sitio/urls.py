from django.urls import path
from .views import *


urlpatterns = [
    path('', sitio_view, name='sitio'),
    path('agregar/', agrega_sitio_view, name='agrega_configuracion'),
    #path('edita/', edita_sitio_view, name='edita_configuracion'),
    path('borra/<int:id>/', borra_sitio_view, name='borra_configuracion'),
]
