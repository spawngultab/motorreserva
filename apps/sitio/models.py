from django.db import models


# DATOS DE CONFIGURACIÓN DE CNTS WEB
class Sitio(models.Model):
	IDIOMAS_DISPONIBLES = (
		('', ''),
        ('ESP', 'Español'),
        ('ING', 'Inglés'),
    )
	nombre_configuracion = models.CharField(max_length=50, default='default', unique=True)
	# COLORES
	logo = models.ImageField(upload_to='sitio/', blank=True, null=True)
	color_fondo = models.CharField(max_length=7, default='#f5f7fb')
	color_fondo_sidebar = models.CharField(max_length=7, default='#0078d4')
	color_fondo_navbar = models.CharField(max_length=7, default='#ffffff')
	color_fondo_footer = models.CharField(max_length=7, default='#ffffff')
	# IDIOMAS
	idioma = models.CharField(max_length=3, choices=IDIOMAS_DISPONIBLES, default='ESP')
	status = models.BooleanField(default=False)

	class Meta:
		verbose_name = 'Configuración'
		verbose_name_plural = 'Configuraciones'

	def __str__(self):
		return self.nombre_configuracion

