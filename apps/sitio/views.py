from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Sitio
from .forms import SitioForm


# Falta funcion para cargar los datos de configuracion en los templates
@login_required
def sitio_view(request):
	consulta = list(Sitio.objects.all().values())
	return render(request, 'configuracion/configura_sitio.html', {'consulta': consulta})


@login_required
def agrega_sitio_view(request):
	if request.method == 'POST':
		form = SitioForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('sitio')
	else:
		form = SitioForm()
	return render(request, 'configuracion/agrega_sitio.html', {'form': form})

"""
def edita_sitio_view(request):
	try:
		datos = HttpRequest.read(request)
		datos_decodificados = datos.decode('utf-8')
		datos_separados = datos_decodificados.split(',')
		#nombre_configuracion_form = datos['nombre_configuracion']
		print(datos_separados[0].strip())
		#consulta_configuracion = ConfiguracionSitio.objects.filter(nombre_configuracion=nombre_configuracion_form).values()
		#return JsonResponse({'respuesta': list(consulta_configuracion)}, safe=False)
	except Exception as e:
		print(e)

	if request.method == 'POST':
		form = ConfiguracionSitioForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('sitio')
	else:
		form = ConfiguracionSitioForm()
	return render(request, 'configuracion/edita_sitio.html', {'form': form})
"""

@login_required
def borra_sitio_view(request, id):
	SitioForm.objects.filter(id=id).delete()
	return redirect('sitio')
