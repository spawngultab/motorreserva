from django.contrib import admin
from .models import Habitacion, Foto


admin.site.register(Habitacion)
admin.site.register(Foto)
