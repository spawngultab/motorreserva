from django import forms
from .models import Habitacion, Foto


class HabitacionForm(forms.ModelForm):
	class Meta:
		model = Habitacion
		fields = '__all__'
		widgets = {
			'clave': forms.TextInput(attrs={'class': 'form-control'}),
			'nombre': forms.TextInput(attrs={'class': 'form-control'}),
			'descripcion': forms.TextInput(attrs={'class': 'form-control'}),
			'amenidades': forms.TextInput(attrs={'class': 'form-control'}),
		}


class FotoForm(forms.ModelForm):
	class Meta:
		model = Foto
		fields = '__all__'
		widgets = {
			'habitacion': forms.TextInput(attrs={'class': 'form-control'}),
			'foto': forms.ClearableFileInput(),
		}
