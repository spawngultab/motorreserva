from django.urls import path
from .views import *


urlpatterns = [
    path('', habitacion_view, name='habitacion'),
    path('agregar/', agrega_habitacion_view, name='agrega_habitacion'),
    path('agregar/foto/habitacion/<int:id>/', agrega_foto_view, name='agrega_foto'),
    path('editar/<int:id>/', edita_habitacion_view, name='edita_habitacion'),
    path('borrar/<int:id>/', borra_habitacion_view, name='borra_habitacion'),
]
