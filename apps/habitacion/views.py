from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from apps.configuracion.models import TabEst
from apps.configuracion.forms import TabEstForm
from .models import Habitacion, Foto
from .forms import HabitacionForm, FotoForm


"""
@login_required
def habitacion_view(request):
	# SELECT * FROM TAB_EST WHERE TYPTAB = "A";
	consulta = list(TabEst.objects.filter(typtab="A").values('id', 'clave', 'descrip'))
	return render(request, 'configuracion/configura_habitacion.html', {'lista_habitaciones': consulta})

@login_required
def agrega_habitacion_view(request):
	if request.method == 'POST':
		form = TabEstForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('habitacion')
	else:
		form = TabEstForm()
	return render(request, 'configuracion/agrega_habitacion.html', {'form': form})
"""
@login_required
def habitacion_view(request):
	#consulta_habitaciones_cnts = list(TabEst.objects.filter(typtab='A').values('id', 'clave', 'descrip'))
	#for item in range(consulta_habitaciones_cnts):
		#print(consulta_habitaciones_cnts[item]['clave'])
	"""
	consulta_cantidad_habitaciones_cnts = Habitacion.objects.all().count()
	if not consulta_cantidad_habitaciones_cnts == 0:
		consulta = list(Habitacion.objects.all().values('id', 'clave', 'nombre', 'descripcion', 'amenidades'))
	else:
		consulta_habitaciones_cnts = list(TabEst.objects.filter(typtab='A').values('id', 'clave', 'descrip'))
		print(consulta_habitaciones_cnts)
		for item in consulta_habitaciones_cnts[0]:
			print(consulta_habitaciones_cnts[0][item])
	"""
	consulta = list(Habitacion.objects.all().values('id', 'clave', 'nombre', 'descripcion', 'amenidades'))
	return render(request, 'configuracion/configura_habitacion.html', {'lista_habitaciones': consulta})


@login_required
def agrega_habitacion_view(request):
	if request.method == 'POST':
		form = HabitacionForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('habitacion')
	else:
		form = HabitacionForm()
	return render(request, 'configuracion/agrega_habitacion.html', {'form': form})


@login_required
def edita_habitacion_view(request, id):
	habitacion = Habitacion.objects.get(id=id)
	if request.method == 'POST':
		form = HabitacionForm(request.POST, instance=habitacion)
		if form.is_valid():
			form.save()
			return redirect('habitacion')
	else:
		form = HabitacionForm(instance=habitacion)
	return render(request, 'configuracion/edita_habitacion.html', {'form': form})


@login_required
def borra_habitacion_view(request, id):
	TabEst.objects.filter(id=id).delete()
	return redirect('habitacion')


@login_required
def agrega_foto_view(request, id):
	consulta_habitaciones = list(Habitacion.objects.filter(id=id).values('id', 'clave', 'nombre'))
	consulta_fotos = list(Foto.objects.filter(habitacion_id=id).values('id', 'habitacion_id', 'foto'))
	if request.method == 'POST':
		form = FotoForm(request.POST, request.FILES)
		print(form)
		if form.is_valid():
			form.save()
		return redirect('agrega_foto')
	else:
		form = FotoForm()
	return render(request, 'configuracion/agrega_foto.html', {'form': form, 'id': id, 'habitaciones': consulta_habitaciones, 'fotos': consulta_fotos})
