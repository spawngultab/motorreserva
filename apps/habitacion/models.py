from django.db import models


class Habitacion(models.Model):
	clave = models.CharField(max_length=10)
	nombre = models.CharField(max_length=50, default='')
	descripcion = models.CharField(max_length=100, blank=True, null=True)
	amenidades = models.CharField(max_length=200, blank=True, null=True)

	class Meta:
		verbose_name = 'Habitación'
		verbose_name_plural = 'Habitaciones'

	def __str__(self):
		return self.nombre


class Foto(models.Model):
	habitacion = models.ForeignKey(Habitacion, on_delete=models.CASCADE)
	foto = models.ImageField(upload_to='habitaciones/', blank=True, null=True)

	class Meta:
		verbose_name = 'Foto'
		verbose_name_plural = 'Fotos'

	def __str__(self):
		return self.habitacion.nombre
