import json
from django.shortcuts import render
from django.http import JsonResponse
from apps.edificio.models import Edificio


def reserva_directa_view(request, nombre_corto):
	consulta = Edificio.objects.all().values()
	return render(request, 'checkin_digital/formulario_digital.html', {'datos': consulta})


def genera_numero_reserva_directa(request):
	ceros = {
		1: '0',
		2: '00',
		3: '000',
		4: '0000'
	}

	consulta = list(Reserva.objects.filter(hotel=id_hotel['id_hotel']).order_by('-id').values('numero_prereserva'))
	if consulta:
		numero_reserva                       = int(consulta[0]['numero_prereserva'])
		numero_cadena                        = str(numero_reserva + 1)
		ceros_izquierda                      = 5 - len(numero_cadena)
		numero                               = ceros.get(ceros_izquierda) + numero_cadena
		request.session['numero_prereserva'] = numero
		request.session['fecha_hora']        = str(datetime.datetime.now())
	else:
		numero                               = '00001'
		request.session['numero_prereserva'] = numero
		request.session['fecha_hora']        = str(datetime.datetime.now())
	return JsonResponse(numero, safe=False)

"""
def reserva_directa_view(request, hotel_id):
	# Formulario de captura de nuevas pre-reservas directas por el cliente
	consulta       = list(Hotel.objects.filter(id=hotel_id).values('logo', 'nombre', 'nombre_empresa'))
	logo           = consulta[0]['logo']
	nombre         = consulta[0]['nombre']
	nombre_empresa = consulta[0]['nombre_empresa']
	if request.method == 'POST':
		paterno = request.POST.get('apellido_paterno')
		materno = request.POST.get('apellido_materno')
		nombre  = request.POST.get('nombre')
		correo  = request.POST.get('email')
		reserva = request.POST.get('numero_prereserva')
		form    = ReservaForm(request.POST)
		if form.is_valid():
			form.save()
			if is_valid_email(correo):
				enviaMailDirecto(correo, hotel_id, reserva)
			else:
				return redirect('error_reserva_directa', hotel_id=hotel_id, reserva=reserva)
		return redirect('confirma_reserva_directa', hotel_id=hotel_id, reserva=reserva)
	else:
		form = ReservaForm()
	return render(request, 'reserva/reserva_directa.html', {'form': form, 'hotel_id': hotel_id, 'logo': logo, 'nombre': nombre, 'nombre_empresa': nombre_empresa})


def confirma_reserva_directa_view(request, hotel_id, reserva):
	return render(request, 'reserva/confirma_reserva_directa.html', {'hotel_id': hotel_id, 'reserva': reserva})
"""