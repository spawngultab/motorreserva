from django.urls import path
from .views import *


urlpatterns = [
    path('<str:nombre_corto>/', reserva_directa_view, name='reserva_directa'),
    path('genera_numero_reserva_directa/', genera_numero_reserva_directa, name='genera_numero_reserva_directa')
]
