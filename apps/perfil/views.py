from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .models import Perfil


@login_required
def perfil_view(request):
	usuario = request.user
	consulta = list(Perfil.objects.filter(usuario=usuario).values('id', 'usuario_id', 'usuario_id__username', 'clave', 'tipo', 'avatar', 'puesto', 'direccion', 'pais', 'telefono', 'hotel__nombre_corto'))
	return render(request, 'perfil/perfil.html', {'perfil': consulta})
