from django.contrib.auth.models import User
from django.db import models
from apps.edificio.models import Edificio


class Perfil(models.Model):
	PERMISO_SISTEMA = (
		('user', 'usuario'),
		('admin', 'administrador'),
	)

	PUESTO = (
		('Recepcionista', 'Recepcionista'),
		('Gerente', 'Gerente'),
		('Sistemas', 'Sistemas'),
	)

	usuario = models.OneToOneField(User, on_delete=models.CASCADE)
	clave = models.CharField(max_length=4, blank=True, null=True)
	tipo = models.CharField(max_length=5, choices=PERMISO_SISTEMA, default='user')
	avatar = models.ImageField(upload_to='avatar/', blank=True, null=True)
	puesto = models.CharField(max_length=15, choices=PUESTO, blank=True, null=True)
	direccion = models.CharField(max_length=200, blank=True, null=True)
	pais = models.CharField(max_length=15, blank=True, null=True)
	telefono = models.CharField(max_length=10, blank=True, null=True)
	hotel = models.ForeignKey(Edificio, on_delete=models.CASCADE, blank=True, null=True)

	class Meta:
		verbose_name = 'Perfil'
		verbose_name_plural = 'Perfiles'

	def __str__(self):
		return self.usuario.username
